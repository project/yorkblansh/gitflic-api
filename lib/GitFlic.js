'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.GitFlic = void 0
const API_1 = require('./api/API')
const OAUTH_1 = require('./oauth/OAUTH')
/**
Новый инстанс GitFlic принимает обьект:
```ts
- const gitflic_credentials: {
-	gitflic_api_url: string //url адрес gitflic api
-	gitflic_token: string   //токен
- } as GitFlicCredentials
```
 */
class GitFlic {
	gc
	API
	static OAUTH = OAUTH_1.OAUTH
	constructor(gc) {
		this.gc = gc
		this.API = new API_1.API(this.gc)
	}
}
exports.GitFlic = GitFlic
//# sourceMappingURL=GitFlic.js.map
