import { Commit } from '../commit.interface'
export interface FileAlter {
	id: string
	newMode: string
	oldMode: string
	newPath: string
	oldPath: string
	lastCommit: Commit
	addedLinesCount: number
	removedLinesCount: number
	fileName: string
	filePath: string
	fileExtension: string
	isImg: boolean
	isCollapsed: boolean
	isLarge: boolean
	isBinary: boolean
	changeType: string
	headers: string[]
	lines: FileAlterLine[]
}
