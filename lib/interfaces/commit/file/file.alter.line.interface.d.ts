declare type FileLineType = 'separator' | 'line'
declare type FileLineOP = 'none' | 'remove' | 'add'
interface FileAlterLine {
	id: string
	body: string
	highLightBody: string
	addLineNumber: null | number
	removeLineNumber: null | number
	op: FileLineOP
	type: FileLineType
}
