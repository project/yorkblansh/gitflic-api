export interface Author_or_CommitterIdent {
	name: string
	avatar: string
	emailAddress: string
	when: Date
}
