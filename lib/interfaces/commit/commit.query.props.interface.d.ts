export declare type CommitQueryProps = {
	params: {
		userAlias: string
		projectAlias: string
		commitId: string
		filePath?: string
	}
}
