export interface ReleasePerformQueryProps {
	title: string
	description: string
	tagName: string
	preRelease: boolean
}
