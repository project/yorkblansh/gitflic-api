export interface ReleaseQueryProps {
	ownerAlias: string
	projectAlias: string
	releaseUuid: string
	fileName: string
}
