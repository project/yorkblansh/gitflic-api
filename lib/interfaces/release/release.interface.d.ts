import { AttachmentFile } from './release.attachment.file.interface'
export interface Release {
	id: string
	title: string
	projectId: string
	authorId: string
	description: string
	tagName: string
	commitId: string
	createdAt: Date
	updatedAt: Date
	attachmentFiles: AttachmentFile[]
	preRelease: boolean
}
