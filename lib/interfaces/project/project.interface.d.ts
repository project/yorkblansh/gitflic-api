export interface Project {
	id: string
	title: string
	description: string
	alias: string
	ownerAlias: string
	httpTransportUrl: string
	sshTransportUrl: string
	selectorTitle: string
	private: boolean
	selectorOwnerAlias: string
	selectorId: string
	selectorAlias: string
	selectorColor: string
	selectorHash: string
}
