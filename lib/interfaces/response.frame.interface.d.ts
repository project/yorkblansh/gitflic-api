import { Team } from './team/team.interface'
import { Project } from './project/project.interface'
import { User } from './user/user.interface'
import { Company } from './company/company.interface'
import { IssueDiscussion } from './issue-discussion/issue.discussion.interface'
import { Release } from './release/release.interface'
import { MergeRequestModel } from './merge-request/merge.request.model.interface'
export interface EntitieList<T extends keyof _embeddedTypes> {
	_embedded: _embedded<T>
	page: {
		size: number
		totalElements: number
		totalPages: number
		number: number
	}
}
export interface _embeddedTypes {
	projectList: Project[]
	userList: User[]
	teamList: Team[]
	companyList: Company[]
	issueDiscussionList: IssueDiscussion[]
	releaseTagModelList: Release[]
	mergeRequestModelList: MergeRequestModel[]
}
export declare type _embedded<T extends keyof _embeddedTypes> = {
	[each in _embeddedTypes as T]: _embeddedTypes[T]
}
