export interface User {
	id: string
	username: string
	name: string
	surname: string
	fullName: string
	avatar: string
}
