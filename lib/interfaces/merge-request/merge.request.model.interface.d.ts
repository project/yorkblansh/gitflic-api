import { Project } from '../project/project.interface'
import { User } from '../user/user.interface'
import { Branch } from './branch/branch.interface'
import { MergeRequestLabel } from './merge.request.label.interface'
import { MergeRequestStatus } from './merge.request.status.interface'
export interface MergeRequestModel {
	id: string
	localId: number
	description: string
	title: string
	removeSourceBranch: boolean
	squashCommit: boolean
	assignedUsers: User[]
	reviewers: User[]
	labels: MergeRequestLabel[]
	sourceBranch: Branch
	targetBranch: Branch
	status: MergeRequestStatus
	createdBy: User
	createdAt: Date
	updatedAt: Date
	sourceProject: Project
	targetProject: Project
	projectAlias: string
	userAlias: string
	canMerge: boolean
	hasConflicts: boolean
}
