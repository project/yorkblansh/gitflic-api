export interface MergeRequestLabel {
	id: string
	hexColor: string
	title: string
	description: string
	isTextLight: boolean
}
