export interface MergeRequestQueryProps {
	userAlias: string
	projectAlias: string
	localId: number
}
