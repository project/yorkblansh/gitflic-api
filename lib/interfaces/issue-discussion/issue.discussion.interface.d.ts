import { User } from '../user/user.interface'
export interface IssueDiscussion {
	id: string
	message: string
	author: User
	createdAt: Date
	createTimeDifference: string
}
