'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.UserServices = void 0
const USER_MAPS_1 = require('./USER_MAPS')
const curriedFetch_1 = require('../../utils/fetch/curriedFetch')
const userPath_1 = require('../../utils/path/userPath')
class UserServices {
	gc
	_USER_PATH_MAP
	USER_QUERY_MAP
	_fetch
	constructor(gc) {
		this.gc = gc
		const umaps = new USER_MAPS_1.USER_MAPS()
		this._USER_PATH_MAP = umaps.USER_PATH_MAP
		this.USER_QUERY_MAP = (uqmp) => umaps.USER_QUERY_MAP(uqmp)(this)
		this._fetch = (0, curriedFetch_1.curriedFetch)(gc)
	}
	//[x] getUserList()
	getUserList({ options, userQueryType }) {
		const path = this._USER_PATH_MAP()[userQueryType]
		return this._fetch({
			method: 'get',
			options: {
				params: {
					path: (0, userPath_1.userPath)(path ? path : 'me'),
					...options?.params
				}
			}
		})
	}
	//[x] getCurrentUser()
	getCurrentUser({ options, userQueryType }) {
		const isCurrentUser = userQueryType === 'currentUser'
		const path = isCurrentUser && this._USER_PATH_MAP()[userQueryType]
		return this._fetch({
			method: 'get',
			options: {
				params: {
					path: (0, userPath_1.userPath)(path ? path : 'me'),
					...options?.params
				}
			}
		})
	}
	//[ ] getSpecifiedUser()
	getSpecifiedUser() {}
}
exports.UserServices = UserServices
//# sourceMappingURL=user.services.js.map
