import { GitFlicCredentials, EntitieList, User } from '../../interfaces'
import { USER_QUERY_MAP_props } from './typings/user.api.typings'
export declare class UserServices {
	private gc
	private _USER_PATH_MAP
	USER_QUERY_MAP: (uqmp: USER_QUERY_MAP_props) => {
		currentUser: () => unknown
		allUsers: () => unknown
		singleUser: () => unknown
	}
	private _fetch
	constructor(gc: GitFlicCredentials)
	getUserList({
		options,
		userQueryType
	}: USER_QUERY_MAP_props): Promise<
		import('../../../node_modules/axios').AxiosResponse<EntitieList<'userList'>, any>
	>
	getCurrentUser({
		options,
		userQueryType
	}: USER_QUERY_MAP_props): Promise<
		import('../../../node_modules/axios').AxiosResponse<User, any>
	>
	getSpecifiedUser(): void
}
