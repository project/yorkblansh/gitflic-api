'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.UserQueryChain = void 0
class UserQueryChain {
	_USER_AUERY_MAP
	constructor(_USER_AUERY_MAP) {
		this._USER_AUERY_MAP = _USER_AUERY_MAP
	}
	get = (userQueryType) => ({
		by: (options) => this._USER_AUERY_MAP({ options, userQueryType })[userQueryType]()
	})
}
exports.UserQueryChain = UserQueryChain
//# sourceMappingURL=UserQueryChain.js.map
