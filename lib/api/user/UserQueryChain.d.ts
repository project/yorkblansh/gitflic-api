import { AxiosResponse } from '../../axios-from-npm'
import { EntitieList, User } from '../../interfaces'
import { UserQueryType } from './typings/user.api.typings'
import { UserServices } from './user.services'
export declare class UserQueryChain {
	private _USER_AUERY_MAP
	constructor(_USER_AUERY_MAP: UserServices['USER_QUERY_MAP'])
	get: <
		UQT extends UserQueryType,
		UserReturnType = UQT extends 'allUsers' ? EntitieList<'userList'> : User,
		UserQueryOption = UQT extends 'allUsers'
			? import('../../interfaces').Pagination | undefined
			: UQT extends 'currentUser'
			?
					| {
							params?: undefined
					  }
					| undefined
			: {
					params: {
						userAlias: string
					}
			  },
		PromiseReturnType = Promise<AxiosResponse<UserReturnType, any>>
	>(
		userQueryType: UQT
	) => {
		by: (options: UserQueryOption) => PromiseReturnType
	}
}
