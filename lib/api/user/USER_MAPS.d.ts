import { USER_QUERY_MAP_props } from './typings/user.api.typings'
import { UserServices } from './user.services'
export declare class USER_MAPS {
	USER_PATH_MAP: (customPath?: string) => {
		[target: string]: string
	}
	USER_QUERY_MAP: (UQMp: USER_QUERY_MAP_props) => (US: UserServices) => {
		currentUser: () => unknown
		allUsers: () => unknown
		singleUser: () => unknown
	}
}
