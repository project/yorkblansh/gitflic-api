import { Pagination } from '../../../interfaces'
export interface UserQueryOptions {
	userList?: Pagination
	currentUser?: {
		params?: undefined
	}
	specifiedUser: {
		params: {
			userAlias: string
		}
	}
}
export declare type UserQueryOptionsGroup<Name extends keyof UserQueryOptions> =
	UserQueryOptions[Name]
declare type UserListGroup = 'allUsers'
declare type SpecifiedUserGroup = 'singleUser'
declare type CurrentUserGroup = 'currentUser'
export interface UserQueryGroup {
	userList: UserListGroup
	specifiedUser: SpecifiedUserGroup
	currentUser: CurrentUserGroup
}
export declare type UserQueryType = UserListGroup | SpecifiedUserGroup | CurrentUserGroup
export interface USER_QUERY_MAP_props {
	options:
		| UserQueryOptions['userList']
		| UserQueryOptions['currentUser']
		| UserQueryOptions['specifiedUser']
	userQueryType: UserQueryType
}
export {}
