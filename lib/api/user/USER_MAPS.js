'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.USER_MAPS = void 0
class USER_MAPS {
	USER_PATH_MAP = (customPath) => ({
		allUsers: '',
		currentUser: 'me',
		singleUser: customPath ? customPath : 'me'
	})
	USER_QUERY_MAP = (UQMp) => (US) => ({
		allUsers: () => US.getUserList(UQMp),
		currentUser: () => US.getCurrentUser(UQMp)
		//  singleUser: () => US.getSingleUser(UQMp)
	})
}
exports.USER_MAPS = USER_MAPS
//# sourceMappingURL=USER_MAPS.js.map
