import { AxiosResponse } from '../../axios-from-npm'
import { EntitieList } from '../../interfaces'
import { Params } from '../../interfaces/params.generic.interface'
import { Release } from '../../interfaces/release/release.interface'
import { ReleaseQueryProps } from '../../interfaces/release/release.query.props.interface'
import { ReleaseServices } from './release.services'
import { ReleaseQueryType } from './typings/release.api.typings'
export declare class ReleaseQueryChain {
	private S
	constructor(S: ReleaseServices)
	modifyRelease: (
		props: Params<
			ReleaseQueryProps &
				import('../../interfaces/release/release.perform.query.props.interface').ReleasePerformQueryProps
		>
	) => Promise<AxiosResponse<Release, any>>
	createRelease: (
		props: Params<
			ReleaseQueryProps &
				import('../../interfaces/release/release.perform.query.props.interface').ReleasePerformQueryProps
		>
	) => Promise<AxiosResponse<Release, any>>
	deleteRelease: (props: Params<ReleaseQueryProps>) => Promise<AxiosResponse<Release, any>>
	get: <
		RQT extends ReleaseQueryType,
		ReleaseReturnType = RQT extends 'releaseList'
			? EntitieList<'releaseTagModelList'>
			: Release,
		PromiseReturnType = Promise<AxiosResponse<ReleaseReturnType, any>>
	>(
		releaseQueryType: RQT
	) => {
		by: (options: Params<ReleaseQueryProps>) => PromiseReturnType
	}
	private getRelease_MAP
}
