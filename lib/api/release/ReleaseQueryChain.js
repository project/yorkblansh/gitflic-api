'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.ReleaseQueryChain = void 0
class ReleaseQueryChain {
	S
	constructor(S) {
		this.S = S
		this.modifyRelease = this.S.modifyRelease
		this.createRelease = this.S.createRelease
		this.deleteRelease = this.S.deleteRelease
	}
	modifyRelease
	createRelease
	deleteRelease
	get = (releaseQueryType) => ({
		by: (options) => this.getRelease_MAP(options)[releaseQueryType]
	})
	getRelease_MAP = (options) => ({
		singleRelease: this.S.getSingleRelease(options),
		releaseList: this.S.getReleaseList(options)
	})
}
exports.ReleaseQueryChain = ReleaseQueryChain
//# sourceMappingURL=ReleaseQueryChain.js.map
