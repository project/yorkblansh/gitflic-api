import { EntitieList } from '../../interfaces/response.frame.interface'
import { GitFlicCredentials } from '../../interfaces'
import { Release } from '../../interfaces/release/release.interface'
import { Params } from '../../interfaces/params.generic.interface'
import { ReleasePerformQueryProps } from '../../interfaces/release/release.perform.query.props.interface'
import { ReleaseQueryProps } from '../../interfaces/release/release.query.props.interface'
export declare class ReleaseServices {
	private gc
	private fetch
	constructor(gc: GitFlicCredentials)
	getReleaseList(
		props: Params<ReleaseQueryProps>
	): Promise<
		import('../../../node_modules/axios').AxiosResponse<
			EntitieList<'releaseTagModelList'>,
			any
		>
	>
	getSingleRelease(
		props: Params<ReleaseQueryProps>
	): Promise<import('../../../node_modules/axios').AxiosResponse<Release, any>>
	modifyRelease(
		props: Params<ReleaseQueryProps & ReleasePerformQueryProps>
	): Promise<import('../../../node_modules/axios').AxiosResponse<Release, any>>
	createRelease(
		props: Params<ReleaseQueryProps & ReleasePerformQueryProps>
	): Promise<import('../../../node_modules/axios').AxiosResponse<Release, any>>
	deleteRelease(
		props: Params<ReleaseQueryProps>
	): Promise<import('../../../node_modules/axios').AxiosResponse<Release, any>>
	getSingleReleaseFile(): void
	uploadReleaseFileList(): void
	deleteReleaseFileList(): void
	private _releaseOptions
}
