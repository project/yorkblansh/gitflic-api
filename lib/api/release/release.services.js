'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.ReleaseServices = void 0
const curriedFetch_1 = require('../../utils/fetch/curriedFetch')
const releasePath_1 = require('../../utils/path/releasePath')
class ReleaseServices {
	gc
	fetch
	constructor(gc) {
		this.gc = gc
		this.fetch = (0, curriedFetch_1.curriedFetch)(this.gc)
	}
	getReleaseList(props) {
		const path = (0, releasePath_1.releasePath)(props).releaseList()
		const options = this._releaseOptions({ ...props }, path)
		return this.fetch({ method: 'get', options })
	}
	getSingleRelease(props) {
		const path = (0, releasePath_1.releasePath)(props).singleRelease()
		const options = this._releaseOptions({ ...props }, path)
		return this.fetch({ method: 'get', options })
	}
	modifyRelease(props) {
		const path = (0, releasePath_1.releasePath)(props).singleRelease()
		const options = this._releaseOptions({ ...props }, path)
		return this.fetch({ method: 'put', options })
	}
	createRelease(props) {
		const path = (0, releasePath_1.releasePath)(props).singleRelease()
		const options = this._releaseOptions({ ...props }, path)
		return this.fetch({ method: 'post', options })
	}
	deleteRelease(props) {
		const path = (0, releasePath_1.releasePath)(props).singleRelease()
		const options = this._releaseOptions({ ...props }, path)
		return this.fetch({ method: 'delete', options })
	}
	getSingleReleaseFile() {
		// [ ] getSingleReleaseFile()
	}
	uploadReleaseFileList() {
		// [ ] uploadReleaseFileList()
	}
	deleteReleaseFileList() {
		// [ ] deleteReleaseFileList()
	}
	_releaseOptions(props, path) {
		return { params: { ...props.params, path } }
	}
}
exports.ReleaseServices = ReleaseServices
//# sourceMappingURL=release.services.js.map
