import { Params } from '../../../interfaces/params.generic.interface'
import { ReleaseQueryProps } from '../../../interfaces/release/release.query.props.interface'
export interface ReleaseQueryOptions {
	releaseList: Params<ReleaseQueryProps>
	singleRelease: Params<ReleaseQueryProps>
}
export declare type ReleaseQueryOptionsGroup<Name extends keyof ReleaseQueryOptions> =
	ReleaseQueryOptions[Name]
declare type ReleaseListGroup = 'releaseList'
declare type SingleReleaseGroup = 'singleRelease'
export interface ReleaseQueryGroup {
	releaseList: ReleaseListGroup
	singleRelease: SingleReleaseGroup
}
export declare type ReleaseQueryType = ReleaseListGroup | SingleReleaseGroup
export {}
