import { Pagination, IssueDiscussionQueryProps } from '../../interfaces'
import { IssueDiscussionServices } from './issue.discussion.services'
export declare class IssueDiscussionQuery {
	private S
	constructor(S: IssueDiscussionServices)
	getIssueDiscussionList: (
		idqp: IssueDiscussionQueryProps,
		options?: Pagination
	) => Promise<
		import('../../../node_modules/axios').AxiosResponse<
			import('interfaces').EntitieList<'issueDiscussionList'>,
			any
		>
	>
	createIssueDiscussion: (
		idqp: IssueDiscussionQueryProps,
		note: string
	) => Promise<
		import('../../../node_modules/axios').AxiosResponse<
			import('interfaces').IssueDiscussion,
			any
		>
	>
}
