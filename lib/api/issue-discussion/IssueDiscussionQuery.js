'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.IssueDiscussionQuery = void 0
class IssueDiscussionQuery {
	S
	constructor(S) {
		this.S = S
	}
	getIssueDiscussionList = (idqp, options) => this.S.getIssueDiscussionList(idqp, options)
	createIssueDiscussion = (idqp, note) => this.S.createIssueDiscussion(idqp, note)
}
exports.IssueDiscussionQuery = IssueDiscussionQuery
//# sourceMappingURL=IssueDiscussionQuery.js.map
