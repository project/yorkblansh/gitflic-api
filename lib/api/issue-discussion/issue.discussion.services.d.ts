import {
	GitFlicCredentials,
	IssueDiscussion,
	IssueDiscussionQueryProps,
	EntitieList
} from '../../interfaces'
export declare class IssueDiscussionServices {
	private gc
	private _fetch
	constructor(gc: GitFlicCredentials)
	getIssueDiscussionList(
		idqp: IssueDiscussionQueryProps,
		options?: any
	): Promise<
		import('../../../node_modules/axios').AxiosResponse<
			EntitieList<'issueDiscussionList'>,
			any
		>
	>
	createIssueDiscussion(
		idqp: IssueDiscussionQueryProps,
		note: string
	): Promise<import('../../../node_modules/axios').AxiosResponse<IssueDiscussion, any>>
}
