'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.IssueDiscussionServices = void 0
const curriedFetch_1 = require('../../utils/fetch/curriedFetch')
const issueDiscussionPath_1 = require('../../utils/path/issueDiscussionPath')
class IssueDiscussionServices {
	gc
	_fetch
	constructor(gc) {
		this.gc = gc
		this._fetch = (0, curriedFetch_1.curriedFetch)(this.gc)
	}
	//[ ] Указать тип options
	getIssueDiscussionList(idqp, options) {
		const path = (0, issueDiscussionPath_1.issueDiscussionPath)('get', idqp)
		const _options = { params: { path, ...options?.params } }
		return this._fetch({
			method: 'get',
			options: _options
		})
	}
	createIssueDiscussion(idqp, note) {
		// [ ] нужно отладить, протестить, не факт что работает
		const path = (0, issueDiscussionPath_1.issueDiscussionPath)('create', idqp)
		const _options = { params: { path, ...idqp, note } }
		return this._fetch({ method: 'post', options: _options })
	}
}
exports.IssueDiscussionServices = IssueDiscussionServices
//# sourceMappingURL=issue.discussion.services.js.map
