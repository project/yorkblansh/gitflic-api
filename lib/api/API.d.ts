import { GitFlicCredentials } from '../interfaces'
import { CommitQueryChain } from './commit/CommitQueryChain'
import { CompanyQueryChain } from './company/CompanyQueryChain'
import { IssueDiscussionQuery } from './issue-discussion/IssueDiscussionQuery'
import { MergeRequestQueryChain } from './merge-request/MergeRequestQueryChain'
import { ProjectQueryChain } from './project'
import { ReleaseQueryChain } from './release/ReleaseQueryChain'
import { TeamQueryChain } from './team/TeamQueryChain'
import { UserQueryChain } from './user'
export declare class API {
	private gc
	user: UserQueryChain
	project: ProjectQueryChain
	team: TeamQueryChain
	company: CompanyQueryChain
	issueDiscussion: IssueDiscussionQuery
	commit: CommitQueryChain
	release: ReleaseQueryChain
	mergeRequest: MergeRequestQueryChain
	constructor(gc: GitFlicCredentials)
}
