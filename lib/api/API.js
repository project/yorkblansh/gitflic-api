'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.API = void 0
const commit_services_1 = require('./commit/commit.services')
const CommitQueryChain_1 = require('./commit/CommitQueryChain')
const company_services_1 = require('./company/company.services')
const CompanyQueryChain_1 = require('./company/CompanyQueryChain')
const issue_discussion_services_1 = require('./issue-discussion/issue.discussion.services')
const IssueDiscussionQuery_1 = require('./issue-discussion/IssueDiscussionQuery')
const merge_request_services_1 = require('./merge-request/merge.request.services')
const MergeRequestQueryChain_1 = require('./merge-request/MergeRequestQueryChain')
const project_1 = require('./project')
const project_services_1 = require('./project/project.services')
const release_services_1 = require('./release/release.services')
const ReleaseQueryChain_1 = require('./release/ReleaseQueryChain')
const team_services_1 = require('./team/team.services')
const TeamQueryChain_1 = require('./team/TeamQueryChain')
const user_1 = require('./user')
const user_services_1 = require('./user/user.services')
class API {
	gc
	user
	project
	team
	company
	issueDiscussion
	commit
	release
	mergeRequest
	constructor(gc) {
		this.gc = gc
		this.project = new project_1.ProjectQueryChain(
			new project_services_1.ProjectServices(this.gc).PROJECT_QUERY_MAP
		)
		this.user = new user_1.UserQueryChain(
			new user_services_1.UserServices(this.gc).USER_QUERY_MAP
		)
		this.team = new TeamQueryChain_1.TeamQueryChain(
			new team_services_1.TeamServices(this.gc).TEAM_QUERY_MAP
		)
		this.company = new CompanyQueryChain_1.CompanyQueryChain(
			new company_services_1.CompanyServices(this.gc).COMPANY_QUERY_MAP
		)
		this.issueDiscussion = new IssueDiscussionQuery_1.IssueDiscussionQuery(
			new issue_discussion_services_1.IssueDiscussionServices(this.gc)
		)
		this.commit = new CommitQueryChain_1.CommitQueryChain(
			new commit_services_1.CommitServices(this.gc).COMMIT_QUERY_MAP
		)
		this.release = new ReleaseQueryChain_1.ReleaseQueryChain(
			new release_services_1.ReleaseServices(this.gc)
		)
		this.mergeRequest = new MergeRequestQueryChain_1.MergeRequestQueryChain(
			new merge_request_services_1.MergeRequestServices(this.gc)
		)
	}
}
exports.API = API
//# sourceMappingURL=API.js.map
