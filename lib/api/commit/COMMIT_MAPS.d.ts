import { AxiosResponse } from '../../axios-from-npm'
import { CommitQueryProps } from '../../interfaces/commit/commit.query.props.interface'
import { CommitServices } from './commit.services'
export declare class COMMIT_MAPS {
	COMMIT_QUERY_MAP: (cqt: CommitQueryProps) => (CS: CommitServices) => {
		commit: () => Promise<AxiosResponse<any, any>>
		fileCommit: () => Promise<AxiosResponse<any, any>>
		affectedFileList: () => Promise<AxiosResponse<any, any>>
		affectedFile: () => Promise<AxiosResponse<any, any>>
	}
}
