'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.COMMIT_MAPS = void 0
class COMMIT_MAPS {
	// public COMMIT_PATH_MAP = (customPath?: string) =>
	// 	({
	// 		affectedFileList: '',
	// 		commit: 'my',
	// 		fileCommit: customPath ? customPath : 'my',
	// 		affectedFile
	// 	} as { [each in CommitQueryType]: string })
	COMMIT_QUERY_MAP = (cqt) => (CS) => ({
		affectedFile: () => CS.getAffectedFile(cqt),
		affectedFileList: () => CS.getAffectedFileList(cqt),
		commit: () => CS.getCommit(cqt),
		fileCommit: () => CS.getFileCommit(cqt)
	})
}
exports.COMMIT_MAPS = COMMIT_MAPS
//# sourceMappingURL=COMMIT_MAPS.js.map
