import { AxiosResponse } from '../../axios-from-npm'
import { Commit } from '../../interfaces/commit/commit.interface'
import { CommitQueryProps } from '../../interfaces/commit/commit.query.props.interface'
import { FileAlter } from '../../interfaces/commit/file/file.alter.interface'
import { CommitServices } from './commit.services'
import { CommitQueryType } from './typings/commit.api.typings'
export declare class CommitQueryChain {
	private _COMMIT_QUERY_MAP
	constructor(_COMMIT_QUERY_MAP: CommitServices['COMMIT_QUERY_MAP'])
	get: <
		CQT extends CommitQueryType,
		ReturnType_1 = CQT extends 'commit' | 'fileCommit'
			? Commit
			: CQT extends 'affectedFile'
			? FileAlter
			: FileAlter[],
		PromiseReturnType = Promise<AxiosResponse<ReturnType_1, any>>
	>(
		commitQueryType: CQT
	) => {
		by: (options: CommitQueryProps) => PromiseReturnType
	}
}
