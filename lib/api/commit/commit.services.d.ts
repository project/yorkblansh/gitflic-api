import { GitFlicCredentials } from '../../interfaces'
import { Commit } from '../../interfaces/commit/commit.interface'
import { CommitQueryProps } from '../../interfaces/commit/commit.query.props.interface'
import { FileAlter } from '../../interfaces/commit/file/file.alter.interface'
export declare class CommitServices {
	private gc
	COMMIT_QUERY_MAP: (cqt: CommitQueryProps) => {
		commit: () => Promise<import('../../../node_modules/axios').AxiosResponse<any, any>>
		fileCommit: () => Promise<import('../../../node_modules/axios').AxiosResponse<any, any>>
		affectedFileList: () => Promise<
			import('../../../node_modules/axios').AxiosResponse<any, any>
		>
		affectedFile: () => Promise<
			import('../../../node_modules/axios').AxiosResponse<any, any>
		>
	}
	private fetch
	constructor(gc: GitFlicCredentials)
	getCommit(
		props: CommitQueryProps
	): Promise<import('../../../node_modules/axios').AxiosResponse<Commit, any>>
	getAffectedFileList(
		props: CommitQueryProps
	): Promise<import('../../../node_modules/axios').AxiosResponse<FileAlter[], any>>
	getAffectedFile(
		props: CommitQueryProps
	): Promise<import('../../../node_modules/axios').AxiosResponse<FileAlter, any>>
	getFileCommit(
		props: CommitQueryProps
	): Promise<import('../../../node_modules/axios').AxiosResponse<Commit, any>>
}
