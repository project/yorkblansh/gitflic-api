'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.CommitServices = void 0
const curriedFetch_1 = require('../../utils/fetch/curriedFetch')
const commitPath_1 = require('../../utils/path/commitPath')
const COMMIT_MAPS_1 = require('./COMMIT_MAPS')
class CommitServices {
	gc
	COMMIT_QUERY_MAP
	fetch
	constructor(gc) {
		this.gc = gc
		const cmaps = new COMMIT_MAPS_1.COMMIT_MAPS()
		this.COMMIT_QUERY_MAP = (cqt) => cmaps.COMMIT_QUERY_MAP(cqt)(this)
		this.fetch = (0, curriedFetch_1.curriedFetch)(this.gc)
	}
	getCommit(props) {
		const path = (0, commitPath_1.commitPath)(props).commit()
		const options = { params: { path, ...props.params } }
		return this.fetch({ method: 'get', options })
	}
	getAffectedFileList(props) {
		const path = (0, commitPath_1.commitPath)(props).affectedFileList(props.params.filePath)
		const options = { params: { path, ...props.params } }
		return this.fetch({ method: 'get', options })
	}
	getAffectedFile(props) {
		const path = (0, commitPath_1.commitPath)(props).affectedFile(props.params.filePath)
		const options = { params: { path, ...props.params } }
		return this.fetch({ method: 'get', options })
	}
	getFileCommit(props) {
		const path = (0, commitPath_1.commitPath)(props).fileCommit(props.params.filePath)
		const options = { params: { path, ...props.params } }
		return this.fetch({ method: 'get', options })
	}
}
exports.CommitServices = CommitServices
//# sourceMappingURL=commit.services.js.map
