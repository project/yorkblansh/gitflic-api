import { CommitQueryProps } from '../../../interfaces/commit/commit.query.props.interface'
export interface CommitQueryOptions {
	list: CommitQueryProps
	commit: CommitQueryProps
}
export declare type CommitQueryOptionsGroup<Name extends keyof CommitQueryOptions> =
	CommitQueryOptions[Name]
declare type FileAlterListGroup = 'affectedFileList'
declare type SingleCommitGroup = 'commit' | 'fileCommit'
declare type SingleFileAlter = 'affectedFile'
export interface CommitQueryGroup {
	fileAlterList: FileAlterListGroup
	commit: SingleCommitGroup
	fileAlter: SingleFileAlter
}
export declare type CommitQueryType = FileAlterListGroup | SingleCommitGroup | SingleFileAlter
export {}
