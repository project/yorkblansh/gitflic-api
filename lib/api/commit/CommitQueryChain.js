'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.CommitQueryChain = void 0
class CommitQueryChain {
	_COMMIT_QUERY_MAP
	constructor(_COMMIT_QUERY_MAP) {
		this._COMMIT_QUERY_MAP = _COMMIT_QUERY_MAP
	}
	get = (commitQueryType) => ({
		by: (options) => this._COMMIT_QUERY_MAP(options)[commitQueryType]()
	})
}
exports.CommitQueryChain = CommitQueryChain
//# sourceMappingURL=CommitQueryChain.js.map
