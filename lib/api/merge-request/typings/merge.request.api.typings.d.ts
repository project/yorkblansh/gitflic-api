import { MergeRequestQueryProps } from '../../../interfaces/merge-request/merge.request.query.props.interface'
import { Params } from '../../../interfaces/params.generic.interface'
export interface MergeRequestQueryOptions {
	releaseList: Params<MergeRequestQueryProps>
	singleMergeRequest: Params<MergeRequestQueryProps>
}
export declare type MergeRequestQueryOptionsGroup<Name extends keyof MergeRequestQueryOptions> =
	MergeRequestQueryOptions[Name]
declare type MergeRequestListGroup = 'mrList'
declare type SingleMergeRequestGroup = 'singleMR'
export interface MergeRequestQueryGroup {
	list: MergeRequestListGroup
	single: SingleMergeRequestGroup
}
export declare type MRQueryType = MergeRequestListGroup | SingleMergeRequestGroup
export {}
