'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.MergeRequestServices = void 0
const curriedFetch_1 = require('../../utils/fetch/curriedFetch')
const mergeRequestPath_1 = require('../../utils/path/mergeRequestPath')
class MergeRequestServices {
	gc
	fetch
	constructor(gc) {
		this.gc = gc
		this.fetch = (0, curriedFetch_1.curriedFetch)(this.gc)
	}
	getList(props) {
		const options = this.options({ ...props, queryType: 'list' })
		return this.fetch({ method: 'get', options })
	}
	getSingle(props) {
		const options = this.options({ ...props, queryType: 'single' })
		return this.fetch({ method: 'get', options })
	}
	modify(props) {
		const options = this.options({ ...props, queryType: 'modify' })
		return this.fetch({ method: 'put', options })
	}
	create(props) {
		const options = this.options({ ...props, queryType: 'create' })
		return this.fetch({ method: 'post', options })
	}
	perform(props) {
		const options = this.options({ ...props, queryType: 'perform' })
		return this.fetch({ method: 'post', options })
	}
	close(props) {
		const options = this.options({ ...props, queryType: 'close' })
		return this.fetch({ method: 'post', options })
	}
	cancel(props) {
		const options = this.options({ ...props, queryType: 'cancel' })
		return this.fetch({ method: 'post', options })
	}
	options(props) {
		const path = (0, mergeRequestPath_1.mergeRequestPath)(props)[props.queryType]()
		return { params: { ...props.params, path } }
	}
}
exports.MergeRequestServices = MergeRequestServices
//# sourceMappingURL=merge.request.services.js.map
