import { EntitieList, GitFlicCredentials } from '../../interfaces'
import { MergeRequestModel } from '../../interfaces/merge-request/merge.request.model.interface'
import { MergeRequestQueryProps } from '../../interfaces/merge-request/merge.request.query.props.interface'
import { MergeRequestPerformQueryProps } from '../../interfaces/merge-request/query.props/merge.request.perform.query.props.interface'
import { Params } from '../../interfaces/params.generic.interface'
export declare class MergeRequestServices {
	private gc
	private fetch
	constructor(gc: GitFlicCredentials)
	getList(
		props: Params<MergeRequestQueryProps>
	): Promise<
		import('../../../node_modules/axios').AxiosResponse<
			EntitieList<'mergeRequestModelList'>,
			any
		>
	>
	getSingle(
		props: Params<MergeRequestQueryProps>
	): Promise<import('../../../node_modules/axios').AxiosResponse<MergeRequestModel, any>>
	modify(
		props: Params<MergeRequestQueryProps & MergeRequestPerformQueryProps>
	): Promise<import('../../../node_modules/axios').AxiosResponse<MergeRequestModel, any>>
	create(
		props: Params<MergeRequestQueryProps & MergeRequestPerformQueryProps>
	): Promise<import('../../../node_modules/axios').AxiosResponse<MergeRequestModel, any>>
	perform(
		props: Params<MergeRequestQueryProps & MergeRequestPerformQueryProps>
	): Promise<import('../../../node_modules/axios').AxiosResponse<MergeRequestModel, any>>
	close(
		props: Params<MergeRequestQueryProps & MergeRequestPerformQueryProps>
	): Promise<import('../../../node_modules/axios').AxiosResponse<MergeRequestModel, any>>
	cancel(
		props: Params<MergeRequestQueryProps & MergeRequestPerformQueryProps>
	): Promise<import('../../../node_modules/axios').AxiosResponse<MergeRequestModel, any>>
	private options
}
