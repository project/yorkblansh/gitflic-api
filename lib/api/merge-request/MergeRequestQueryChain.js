'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.MergeRequestQueryChain = void 0
class MergeRequestQueryChain {
	S
	constructor(S) {
		this.S = S
		this.modify = this.S.modify
		this.create = this.S.create
		this.cancel = this.S.cancel
		this.close = this.S.close
	}
	modify
	create
	cancel
	close
	get(releaseQueryType) {
		return {
			by: (options) => this.SERVICE_MAP(options)[releaseQueryType]
		}
	}
	SERVICE_MAP = (options) => ({
		singleMR: this.S.getSingle(options),
		mrList: this.S.getList(options)
	})
}
exports.MergeRequestQueryChain = MergeRequestQueryChain
//# sourceMappingURL=MergeRequestQueryChain.js.map
