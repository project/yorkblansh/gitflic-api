import { AxiosResponse } from '../../axios-from-npm'
import { EntitieList } from '../../interfaces'
import { MergeRequestModel } from '../../interfaces/merge-request/merge.request.model.interface'
import { MergeRequestQueryProps } from '../../interfaces/merge-request/merge.request.query.props.interface'
import { Params } from '../../interfaces/params.generic.interface'
import { MergeRequestServices } from './merge.request.services'
import { MergeRequestQueryGroup, MRQueryType } from './typings/merge.request.api.typings'
export declare class MergeRequestQueryChain {
	private S
	constructor(S: MergeRequestServices)
	modify: (
		props: Params<
			MergeRequestQueryProps &
				import('../../interfaces/merge-request/query.props/merge.request.perform.query.props.interface').MergeRequestPerformQueryProps
		>
	) => Promise<AxiosResponse<MergeRequestModel, any>>
	create: (
		props: Params<
			MergeRequestQueryProps &
				import('../../interfaces/merge-request/query.props/merge.request.perform.query.props.interface').MergeRequestPerformQueryProps
		>
	) => Promise<AxiosResponse<MergeRequestModel, any>>
	cancel: (
		props: Params<
			MergeRequestQueryProps &
				import('../../interfaces/merge-request/query.props/merge.request.perform.query.props.interface').MergeRequestPerformQueryProps
		>
	) => Promise<AxiosResponse<MergeRequestModel, any>>
	close: (
		props: Params<
			MergeRequestQueryProps &
				import('../../interfaces/merge-request/query.props/merge.request.perform.query.props.interface').MergeRequestPerformQueryProps
		>
	) => Promise<AxiosResponse<MergeRequestModel, any>>
	get<
		RQT extends MRQueryType,
		ReturnType = RQT extends MergeRequestQueryGroup['list']
			? EntitieList<'mergeRequestModelList'>
			: MergeRequestModel,
		PromiseReturnType = Promise<AxiosResponse<ReturnType, any>>
	>(
		releaseQueryType: RQT
	): {
		by: (options: Params<MergeRequestQueryProps>) => PromiseReturnType
	}
	private SERVICE_MAP
}
