import { Project } from '../../interfaces/project/project.interface'
import { EntitieList } from '../../interfaces/response.frame.interface'
import { PROJECT_QUERY_MAP_props } from './typings/project.api.typings'
import { GitFlicCredentials } from '../../interfaces'
export declare class ProjectServices {
	private gc
	private _PROJECT_PATH_MAP
	PROJECT_QUERY_MAP: (tmp: PROJECT_QUERY_MAP_props) => {
		singleProject: () => unknown
		allProjects: () => unknown
		myProjects: () => unknown
		sharedProjects: () => unknown
	}
	private _fetch
	constructor(gc: GitFlicCredentials)
	getProjectList({
		options,
		projectQueryType
	}: PROJECT_QUERY_MAP_props): Promise<
		import('../../../node_modules/axios').AxiosResponse<EntitieList<'projectList'>, any>
	>
	getSingleProject({
		options,
		projectQueryType
	}: PROJECT_QUERY_MAP_props): Promise<
		import('../../../node_modules/axios').AxiosResponse<Project, any>
	>
}
