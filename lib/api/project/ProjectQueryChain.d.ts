import { ProjectServices } from './project.services'
import { Project, EntitieList } from '../../interfaces'
import { ProjectQueryType } from './typings/project.api.typings'
import { AxiosResponse } from '../../axios-from-npm'
export declare class ProjectQueryChain {
	private _PROJECT_QUERY_MAP
	constructor(_PROJECT_QUERY_MAP: ProjectServices['PROJECT_QUERY_MAP'])
	get: <
		PQT extends ProjectQueryType,
		ProjectReturnType = PQT extends 'allProjects' | 'myProjects' | 'sharedProjects'
			? EntitieList<'projectList'>
			: Project,
		PQO = PQT extends 'allProjects' | 'myProjects' | 'sharedProjects'
			? import('../../interfaces').Pagination | undefined
			: {
					params: import('./typings/project.api.typings').UserSpecification
			  },
		PromiseReturnType = Promise<AxiosResponse<ProjectReturnType, any>>
	>(
		projectQueryType: PQT
	) => {
		by: (options: PQO) => PromiseReturnType
	}
}
