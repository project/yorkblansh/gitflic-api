import { Pagination } from '../../../interfaces'
export declare type UserSpecification = {
	userAlias: string
	projectAlias: string
}
export interface ProjectQueryOptions {
	projectList?: Pagination
	singleProject: {
		params: UserSpecification
	}
}
export declare type ProjectQueryOptionsGroup<Name extends keyof ProjectQueryOptions> =
	ProjectQueryOptions[Name]
declare type ProjectListGroup = 'allProjects' | 'myProjects' | 'sharedProjects'
declare type SingleProjectGroup = 'singleProject'
export interface ProjectQueryGroup {
	projectList: ProjectListGroup
	singleProject: SingleProjectGroup
}
export declare type ProjectQueryType = ProjectListGroup | SingleProjectGroup
export interface PROJECT_QUERY_MAP_props {
	options: ProjectQueryOptions['projectList'] | ProjectQueryOptions['singleProject']
	projectQueryType: ProjectQueryType
}
export {}
