'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.ProjectQueryChain = void 0
class ProjectQueryChain {
	_PROJECT_QUERY_MAP
	constructor(_PROJECT_QUERY_MAP) {
		this._PROJECT_QUERY_MAP = _PROJECT_QUERY_MAP
	}
	get = (projectQueryType) => ({
		by: (options) =>
			this._PROJECT_QUERY_MAP({
				options,
				projectQueryType
			})[projectQueryType]()
	})
}
exports.ProjectQueryChain = ProjectQueryChain
//# sourceMappingURL=ProjectQueryChain.js.map
