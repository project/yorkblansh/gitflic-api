'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.PROJECT_MAPS = void 0
class PROJECT_MAPS {
	PROJECT_PATH_MAP = (customPath) => ({
		allProjects: '',
		myProjects: 'my',
		sharedProjects: 'shared',
		singleProject: customPath ? customPath : 'my'
	})
	PROJECT_QUERY_MAP = (PTMp) => (PAS) => ({
		singleProject: () => PAS.getSingleProject(PTMp),
		allProjects: () => PAS.getProjectList(PTMp),
		myProjects: () => PAS.getProjectList(PTMp),
		sharedProjects: () => PAS.getProjectList(PTMp)
	})
}
exports.PROJECT_MAPS = PROJECT_MAPS
//# sourceMappingURL=PROJECT_MAPS.js.map
