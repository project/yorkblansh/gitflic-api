import { PROJECT_QUERY_MAP_props } from './typings/project.api.typings'
import { ProjectServices } from './project.services'
export declare class PROJECT_MAPS {
	PROJECT_PATH_MAP: (customPath?: string) => {
		singleProject: string
		allProjects: string
		myProjects: string
		sharedProjects: string
	}
	PROJECT_QUERY_MAP: (PTMp: PROJECT_QUERY_MAP_props) => (PAS: ProjectServices) => {
		singleProject: () => unknown
		allProjects: () => unknown
		myProjects: () => unknown
		sharedProjects: () => unknown
	}
}
