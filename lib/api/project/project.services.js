'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.ProjectServices = void 0
const project_1 = require('./')
const curriedFetch_1 = require('../../utils/fetch/curriedFetch')
const projectPath_1 = require('../../utils/path/projectPath')
class ProjectServices {
	gc
	_PROJECT_PATH_MAP
	PROJECT_QUERY_MAP
	_fetch
	constructor(gc) {
		this.gc = gc
		const pmaps = new project_1.PROJECT_MAPS()
		this._PROJECT_PATH_MAP = pmaps.PROJECT_PATH_MAP
		this.PROJECT_QUERY_MAP = (tmp) => pmaps.PROJECT_QUERY_MAP(tmp)(this)
		this._fetch = (0, curriedFetch_1.curriedFetch)(this.gc)
	}
	getProjectList({ options, projectQueryType }) {
		const path = this._PROJECT_PATH_MAP()[projectQueryType]
		const _options = {
			params: { path: (0, projectPath_1.projectPath)(path), ...options?.params }
		}
		// console.dir(_options)
		return this._fetch({ method: 'get', options: _options })
	}
	getSingleProject({ options, projectQueryType }) {
		console.dir(options)
		const {
			params: { userAlias, projectAlias }
		} = options
		const path = this._PROJECT_PATH_MAP(
			(0, projectPath_1.projectPath)(`${userAlias}/${projectAlias}`)
		)[projectQueryType]
		console.dir(path)
		const _options = { params: { path, ...options?.params } }
		return this._fetch({ method: 'get', options: _options })
	}
}
exports.ProjectServices = ProjectServices
//# sourceMappingURL=project.services.js.map
