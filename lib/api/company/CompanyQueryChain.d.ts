import { CompanyServices } from './company.services'
import { EntitieList } from '../../interfaces'
import { CompanyQueryType } from './typings/company.api.typings'
import { AxiosResponse } from '../../axios-from-npm'
import { Company } from '../../interfaces/company/company.interface'
export declare class CompanyQueryChain {
	private _COMPANY_QUERY_MAP
	constructor(_COMPANY_QUERY_MAP: CompanyServices['COMPANY_QUERY_MAP'])
	get: <
		PQT extends CompanyQueryType,
		CompanyReturnType = PQT extends 'allCompanies' | 'myCompanies'
			? EntitieList<'companyList'>
			: Company,
		PQO = PQT extends 'allCompanies' | 'myCompanies'
			? import('../../interfaces').Pagination | undefined
			: {
					params: import('./typings/company.api.typings').CompanySpecification
			  },
		PromiseReturnType = Promise<AxiosResponse<CompanyReturnType, any>>
	>(
		companyQueryType: PQT
	) => {
		by: (options: PQO) => PromiseReturnType
	}
}
