import { EntitieList } from '../../interfaces/response.frame.interface'
import { GitFlicCredentials } from '../../interfaces'
import { Company } from '../../interfaces/company/company.interface'
import { COMPANY_QUERY_MAP_props } from './typings/company.api.typings'
export declare class CompanyServices {
	private gc
	private _COMPANY_PATH_MAP
	COMPANY_QUERY_MAP: (CQMp: COMPANY_QUERY_MAP_props) => {
		singleCompany: () => unknown
		allCompanies: () => unknown
		myCompanies: () => unknown
	}
	private _fetch
	constructor(gc: GitFlicCredentials)
	getCompanyList({
		options,
		companyQueryType
	}: COMPANY_QUERY_MAP_props): Promise<
		import('../../../node_modules/axios').AxiosResponse<EntitieList<'companyList'>, any>
	>
	getSingleCompany({
		options,
		companyQueryType
	}: COMPANY_QUERY_MAP_props): Promise<
		import('../../../node_modules/axios').AxiosResponse<Company, any>
	>
}
