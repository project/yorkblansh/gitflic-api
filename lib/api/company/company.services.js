'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.CompanyServices = void 0
const companyPath_1 = require('../../utils/path/companyPath')
const COMPANY_MAPS_1 = require('./COMPANY_MAPS')
const curriedFetch_1 = require('../../utils/fetch/curriedFetch')
class CompanyServices {
	gc
	_COMPANY_PATH_MAP
	COMPANY_QUERY_MAP
	_fetch
	constructor(gc) {
		this.gc = gc
		const cmaps = new COMPANY_MAPS_1.COMPANY_MAPS()
		this._COMPANY_PATH_MAP = cmaps.COMPANY_PATH_MAP
		this.COMPANY_QUERY_MAP = (CQMp) => cmaps.COMPANY_QUERY_MAP(CQMp)(this)
		this._fetch = (0, curriedFetch_1.curriedFetch)(this.gc)
	}
	getCompanyList({ options, companyQueryType }) {
		const path = this._COMPANY_PATH_MAP()[companyQueryType]
		const _options = {
			params: { path: (0, companyPath_1.companyPath)(path), ...options?.params }
		}
		return this._fetch({ method: 'get', options: _options })
	}
	getSingleCompany({ options, companyQueryType }) {
		console.dir(options)
		const {
			params: { companyAlias }
		} = options
		const path = this._COMPANY_PATH_MAP((0, companyPath_1.companyPath)(`${companyAlias}`))[
			companyQueryType
		]
		console.dir(path)
		const _options = { params: { path, ...options?.params } }
		return this._fetch({ method: 'get', options: _options })
	}
}
exports.CompanyServices = CompanyServices
//# sourceMappingURL=company.services.js.map
