'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.COMPANY_MAPS = void 0
class COMPANY_MAPS {
	COMPANY_PATH_MAP = (customPath) => ({
		allCompanies: '',
		myCompanies: 'my',
		singleCompany: customPath ? customPath : 'my'
	})
	COMPANY_QUERY_MAP = (PTMp) => (PAS) => ({
		singleCompany: () => PAS.getSingleCompany(PTMp),
		allCompanies: () => PAS.getCompanyList(PTMp),
		myCompanies: () => PAS.getCompanyList(PTMp)
	})
}
exports.COMPANY_MAPS = COMPANY_MAPS
//# sourceMappingURL=COMPANY_MAPS.js.map
