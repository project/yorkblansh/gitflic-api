'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.CompanyQueryChain = void 0
class CompanyQueryChain {
	_COMPANY_QUERY_MAP
	constructor(_COMPANY_QUERY_MAP) {
		this._COMPANY_QUERY_MAP = _COMPANY_QUERY_MAP
	}
	get = (companyQueryType) => ({
		by: (options) =>
			this._COMPANY_QUERY_MAP({
				options,
				companyQueryType
			})[companyQueryType]()
	})
}
exports.CompanyQueryChain = CompanyQueryChain
//# sourceMappingURL=CompanyQueryChain.js.map
