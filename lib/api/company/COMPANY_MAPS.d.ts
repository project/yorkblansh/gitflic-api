import { COMPANY_QUERY_MAP_props } from './typings/company.api.typings'
import { CompanyServices } from './company.services'
export declare class COMPANY_MAPS {
	COMPANY_PATH_MAP: (customPath?: string) => {
		singleCompany: string
		allCompanies: string
		myCompanies: string
	}
	COMPANY_QUERY_MAP: (PTMp: COMPANY_QUERY_MAP_props) => (PAS: CompanyServices) => {
		singleCompany: () => unknown
		allCompanies: () => unknown
		myCompanies: () => unknown
	}
}
