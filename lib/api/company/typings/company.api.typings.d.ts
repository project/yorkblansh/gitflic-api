import { Pagination } from '../../../interfaces'
export declare type CompanySpecification = {
	companyAlias: string
}
export interface CompanyQueryOptions {
	companyList?: Pagination
	singleCompany: {
		params: CompanySpecification
	}
}
export declare type CompanyQueryOptionsGroup<Name extends keyof CompanyQueryOptions> =
	CompanyQueryOptions[Name]
declare type CompanyListGroup = 'allCompanies' | 'myCompanies'
declare type SingleCompanyGroup = 'singleCompany'
export interface CompanyQueryGroup {
	companyList: CompanyListGroup
	singleCompany: SingleCompanyGroup
}
export declare type CompanyQueryType = CompanyListGroup | SingleCompanyGroup
export interface COMPANY_QUERY_MAP_props {
	options: CompanyQueryOptions['companyList'] | CompanyQueryOptions['singleCompany']
	companyQueryType: CompanyQueryType
}
export {}
