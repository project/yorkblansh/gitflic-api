'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.TEAM_MAPS = void 0
class TEAM_MAPS {
	TEAM_PATH_MAP = (customPath) => ({
		allTeams: '',
		myTeams: 'my',
		sharedTeams: 'shared',
		singleTeam: customPath ? customPath : 'my'
	})
	TEAM_QUERY_MAP = (TQMp) => (TS) => ({
		singleTeam: () => TS.getSingleTeam(TQMp),
		allTeams: () => TS.getTeamList(TQMp),
		myTeams: () => TS.getTeamList(TQMp),
		sharedTeams: () => TS.getTeamList(TQMp)
	})
}
exports.TEAM_MAPS = TEAM_MAPS
//# sourceMappingURL=TEAM_MAPS.js.map
