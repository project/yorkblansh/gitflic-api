'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.TeamServices = void 0
const TEAM_MAPS_1 = require('./TEAM_MAPS')
const curriedFetch_1 = require('../../utils/fetch/curriedFetch')
const teamPath_1 = require('../../utils/path/teamPath')
class TeamServices {
	gc
	_TEAM_PATH_MAP
	TEAM_QUERY_MAP
	_fetch
	constructor(gc) {
		this.gc = gc
		const tmaps = new TEAM_MAPS_1.TEAM_MAPS()
		this._TEAM_PATH_MAP = tmaps.TEAM_PATH_MAP
		this.TEAM_QUERY_MAP = (tmp) => tmaps.TEAM_QUERY_MAP(tmp)(this)
		this._fetch = (0, curriedFetch_1.curriedFetch)(this.gc)
	}
	getTeamList({ options, teamQueryType }) {
		const path = this._TEAM_PATH_MAP()[teamQueryType]
		// [ ] Need refactor | нужно убрать мапы
		const _options = {
			params: { path: (0, teamPath_1.teamPath)(path), ...options?.params }
		}
		return this._fetch({ method: 'get', options: _options })
	}
	getSingleTeam({ options, teamQueryType }) {
		console.dir(options)
		const {
			params: { teamAlias }
		} = options
		// [ ] Need refactor | нужно убрать мапы
		const path = this._TEAM_PATH_MAP((0, teamPath_1.teamPath)(`${teamAlias}`))[
			teamQueryType
		]
		console.dir(path)
		const _options = { params: { path, ...options?.params } }
		return this._fetch({ method: 'get', options: _options })
	}
}
exports.TeamServices = TeamServices
//# sourceMappingURL=team.services.js.map
