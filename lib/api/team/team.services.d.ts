import { EntitieList } from '../../interfaces/response.frame.interface'
import { GitFlicCredentials, Team } from '../../interfaces'
import { TEAM_QUERY_MAP_props } from './typings/team.api.typings'
export declare class TeamServices {
	private gc
	private _TEAM_PATH_MAP
	TEAM_QUERY_MAP: (tmp: TEAM_QUERY_MAP_props) => {
		singleTeam: () => unknown
		allTeams: () => unknown
		myTeams: () => unknown
		sharedTeams: () => unknown
	}
	private _fetch
	constructor(gc: GitFlicCredentials)
	getTeamList({
		options,
		teamQueryType
	}: TEAM_QUERY_MAP_props): Promise<
		import('../../../node_modules/axios').AxiosResponse<EntitieList<'teamList'>, any>
	>
	getSingleTeam({
		options,
		teamQueryType
	}: TEAM_QUERY_MAP_props): Promise<
		import('../../../node_modules/axios').AxiosResponse<Team, any>
	>
}
