import { TeamServices } from './team.services'
import { Team, EntitieList } from '../../interfaces'
import { TeamQueryType } from './typings/team.api.typings'
import { AxiosResponse } from '../../axios-from-npm'
export declare class TeamQueryChain {
	private _TEAM_QUERY_MAP
	constructor(_TEAM_QUERY_MAP: TeamServices['TEAM_QUERY_MAP'])
	get: <
		PQT extends TeamQueryType,
		TeamReturnType = PQT extends 'allTeams' | 'myTeams' | 'sharedTeams'
			? EntitieList<'teamList'>
			: Team,
		PQO = PQT extends 'allTeams' | 'myTeams' | 'sharedTeams'
			? import('../../interfaces').Pagination | undefined
			: {
					params: import('./typings/team.api.typings').TeamSpecification
			  },
		PromiseReturnType = Promise<AxiosResponse<TeamReturnType, any>>
	>(
		teamQueryType: PQT
	) => {
		by: (options: PQO) => PromiseReturnType
	}
}
