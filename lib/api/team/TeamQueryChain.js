'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.TeamQueryChain = void 0
class TeamQueryChain {
	_TEAM_QUERY_MAP
	constructor(_TEAM_QUERY_MAP) {
		this._TEAM_QUERY_MAP = _TEAM_QUERY_MAP
	}
	get = (teamQueryType) => ({
		by: (options) =>
			this._TEAM_QUERY_MAP({
				options,
				teamQueryType
			})[teamQueryType]()
	})
}
exports.TeamQueryChain = TeamQueryChain
//# sourceMappingURL=TeamQueryChain.js.map
