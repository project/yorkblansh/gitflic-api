import { TEAM_QUERY_MAP_props } from './typings/team.api.typings'
import { TeamServices } from './team.services'
export declare class TEAM_MAPS {
	TEAM_PATH_MAP: (customPath?: string) => {
		singleTeam: string
		allTeams: string
		myTeams: string
		sharedTeams: string
	}
	TEAM_QUERY_MAP: (TQMp: TEAM_QUERY_MAP_props) => (TS: TeamServices) => {
		singleTeam: () => unknown
		allTeams: () => unknown
		myTeams: () => unknown
		sharedTeams: () => unknown
	}
}
