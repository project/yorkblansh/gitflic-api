import { Pagination } from '../../../interfaces'
export declare type TeamSpecification = {
	teamAlias: string
}
export interface TeamQueryOptions {
	teamList?: Pagination
	singleTeam: {
		params: TeamSpecification
	}
}
export declare type TeamQueryOptionsGroup<Name extends keyof TeamQueryOptions> =
	TeamQueryOptions[Name]
declare type TeamListGroup = 'allTeams' | 'myTeams' | 'sharedTeams'
declare type SingleTeamGroup = 'singleTeam'
export interface TeamQueryGroup {
	teamList: TeamListGroup
	singleTeam: SingleTeamGroup
}
export declare type TeamQueryType = TeamListGroup | SingleTeamGroup
export interface TEAM_QUERY_MAP_props {
	options: TeamQueryOptions['teamList'] | TeamQueryOptions['singleTeam']
	teamQueryType: TeamQueryType
}
export {}
