'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.releasePath = void 0
const releasePath = ({ params: { ownerAlias, projectAlias, releaseUuid, fileName } }) => {
	const path = `/project/${ownerAlias}/${projectAlias}/release`
	return {
		releaseList: () => `${path}`,
		singleRelease: () => `${path}/${releaseUuid}`,
		singleReleaseFile: () => `${path}/${releaseUuid}/file/${fileName}`
	}
}
exports.releasePath = releasePath
//# sourceMappingURL=releasePath.js.map
