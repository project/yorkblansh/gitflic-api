import { MergeRequestQueryProps } from '../../interfaces/merge-request/merge.request.query.props.interface'
import { Params } from '../../interfaces/params.generic.interface'
export declare function mergeRequestPath({
	params: { userAlias, projectAlias, localId }
}: Params<MergeRequestQueryProps>): {
	list: () => string
	single: () => string
	create: () => string
	modify: () => string
	perform: () => string
	close: () => string
	cancel: () => string
}
