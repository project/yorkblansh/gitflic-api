'use strict'
// import { ReleaseQueryType } from 'api/release/typings/release.api.typings'
// import { Params } from 'interfaces/params.generic.interface'
// import { ReleaseQueryProps } from 'interfaces/release/release.query.props.interface'
Object.defineProperty(exports, '__esModule', { value: true })
exports.mergeRequestPath = void 0
function mergeRequestPath({ params: { userAlias, projectAlias, localId } }) {
	const path = `/project/${userAlias}/${projectAlias}/merge-request`
	return {
		list: () => `${path}/list`,
		single: () => `${path}/${localId}`,
		create: () => `${path}`,
		modify: () => `${path}`,
		perform: () => `${path}/${localId}/merge`,
		close: () => `${path}/${localId}/close`,
		cancel: () => `${path}/${localId}/cancel`
	}
}
exports.mergeRequestPath = mergeRequestPath
//# sourceMappingURL=mergeRequestPath.js.map
