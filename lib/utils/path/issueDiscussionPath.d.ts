import { IssueDiscussionQueryProps } from '../../interfaces'
export declare function issueDiscussionPath(
	queryType: 'get' | 'create',
	{ userAlias, projectAlias, localId }: IssueDiscussionQueryProps
): string
