import { Params } from '../../interfaces/params.generic.interface'
import { ReleaseQueryProps } from '../../interfaces/release/release.query.props.interface'
export declare const releasePath: ({
	params: { ownerAlias, projectAlias, releaseUuid, fileName }
}: Params<ReleaseQueryProps>) => {
	releaseList: (filePath?: string) => string
	singleRelease: (filePath?: string) => string
}
