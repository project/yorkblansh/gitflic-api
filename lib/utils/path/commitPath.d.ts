import { CommitQueryProps } from '../../interfaces/commit/commit.query.props.interface'
export declare const commitPath: ({
	params: { userAlias, projectAlias, commitId }
}: CommitQueryProps) => {
	commit: (filePath?: CommitQueryProps['params']['filePath']) => string
	fileCommit: (filePath?: CommitQueryProps['params']['filePath']) => string
	affectedFileList: (filePath?: CommitQueryProps['params']['filePath']) => string
	affectedFile: (filePath?: CommitQueryProps['params']['filePath']) => string
}
