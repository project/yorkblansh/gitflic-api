'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.issueDiscussionPath = void 0
function issueDiscussionPath(queryType, { userAlias, projectAlias, localId }) {
	// const isGetMethod = queryType === 'get'
	const isCreateMethod = queryType === 'create'
	return `/project/${userAlias}/${projectAlias}/issue-discussion/${localId}/${
		isCreateMethod ? 'create' : ''
	}`
}
exports.issueDiscussionPath = issueDiscussionPath
//# sourceMappingURL=issueDiscussionPath.js.map
