'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.commitPath = void 0
const commitPath = ({ params: { userAlias, projectAlias, commitId } }) => {
	const path = `/project/${userAlias}/${projectAlias}/commit/${commitId}`
	return {
		commit: () => `${path}`,
		affectedFileList: () => `${path}/file`,
		affectedFile: (filePath) => `${path}/diff?filePath=${filePath}`,
		fileCommit: (filePath) => `${path}/for-file?filePath=${filePath}`
	}
}
exports.commitPath = commitPath
//# sourceMappingURL=commitPath.js.map
