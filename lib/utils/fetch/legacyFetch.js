'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.legacyFetch = void 0
const axios_from_npm_1 = require('../../axios-from-npm')
function legacyFetch(fetchMethod, path, legacyProps) {
	const { baseURL, headers, options } = legacyProps
	//TODO clean the code
	// const {
	// 	params: { localId, projectAlias, userAlias, note }
	// } = options as unknown as {
	// 	params: {
	// 		path: string
	// 		localId: 2
	// 		projectAlias: string
	// 		userAlias: string
	// 		note: string
	// 	}
	// }
	console.dir(legacyProps)
	console.dir(path)
	//инстанс axios
	// const response = axios[fetchMethod]<T>(path, { baseURL, headers, ...options })
	//TODO add map object for conditions
	return fetchMethod === 'get'
		? axios_from_npm_1.axios.get(path, { baseURL, headers, ...options })
		: axios_from_npm_1.axios.post(path, options.params, {
				baseURL,
				headers
				// ...options
		  })
	// return response
}
exports.legacyFetch = legacyFetch
//# sourceMappingURL=legacyFetch.js.map
