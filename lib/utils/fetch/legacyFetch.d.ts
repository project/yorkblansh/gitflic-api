import { _axios } from '../../axios-from-npm'
import { Fetch } from './curriedFetch'
export declare type AxiosRequestConfig = _axios.AxiosRequestConfig<any> | undefined
export interface LegacyProps {
	baseURL: string
	headers?: {
		Authorization: string
	}
	options: Fetch['options']
}
export declare function legacyFetch<T>(
	fetchMethod: Fetch['method'],
	path: string,
	legacyProps: LegacyProps
): Promise<_axios.AxiosResponse<T, any>>
