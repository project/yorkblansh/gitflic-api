'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.curriedFetch = void 0
const legacyFetch_1 = require('./legacyFetch')
function curriedFetch({ gitflic_api_url, gitflic_token }) {
	return function ({ method, options }) {
		const { path } = options.params
		const Authorization = `token ${gitflic_token}`
		const legacyProps = {
			baseURL: gitflic_api_url,
			headers: { Authorization },
			options
		}
		return (0, legacyFetch_1.legacyFetch)(method, path, legacyProps)
	}
}
exports.curriedFetch = curriedFetch
//# sourceMappingURL=curriedFetch.js.map
