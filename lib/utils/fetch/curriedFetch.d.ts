import { GitFlicCredentials } from '../../interfaces'
export interface Fetch {
	method: 'get' | 'post' | 'put' | 'delete'
	options: {
		params: {
			path: string
		} & unknown
	}
}
export declare function curriedFetch({
	gitflic_api_url,
	gitflic_token
}: GitFlicCredentials): <ReturnType_1>({
	method,
	options
}: Fetch) => Promise<import('../../../node_modules/axios').AxiosResponse<ReturnType_1, any>>
