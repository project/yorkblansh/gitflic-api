import { API } from './api/API'
import { GitFlicCredentials } from './interfaces'
import { OAUTH } from './oauth/OAUTH'
/**
Новый инстанс GitFlic принимает обьект:
```ts
- const gitflic_credentials: {
-	gitflic_api_url: string //url адрес gitflic api
-	gitflic_token: string   //токен
- } as GitFlicCredentials
```
 */
export declare class GitFlic {
	private gc
	API: API
	static OAUTH: typeof OAUTH
	constructor(gc: GitFlicCredentials)
}
