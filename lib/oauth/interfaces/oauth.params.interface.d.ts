import { Scope } from './scope.enum'
export interface OauthParams {
	scope: Scope[]
	clientId: string
	state: string
	gitflic_oauth_url: string
}
