'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.OAUTH = void 0
class OAUTH {
	clientId
	scope
	state
	gitflic_oauth_url
	constructor({ clientId, scope, state, gitflic_oauth_url }) {
		this.clientId = clientId
		this.scope = scope
		this.state = state
		this.gitflic_oauth_url = gitflic_oauth_url
	}
	getDeepLink() {
		return (
			`${this.gitflic_oauth_url}` +
			`?scope=${this.scope}` +
			`&clientId=${this.clientId}` +
			`&redirectUrl=https://gitflic.ru/` +
			`&state=${this.state}`
		)
	}
}
exports.OAUTH = OAUTH
//# sourceMappingURL=OAUTH.js.map
