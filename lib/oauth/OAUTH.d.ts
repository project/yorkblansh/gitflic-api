import { OauthParams } from './interfaces/oauth.params.interface'
export declare class OAUTH {
	private clientId
	private scope
	private state
	private gitflic_oauth_url
	constructor({ clientId, scope, state, gitflic_oauth_url }: OauthParams)
	getDeepLink(): string
}
