# GitFlic-API

![logo](https://gitflic.ru/project/yorkblansh/gitflic-api/blob/raw?file=docs%2Fimg%2Flib-logo.png&commit=af8097b76ef43539e2d45f0596eb629f287212e4)

# 🧸library provides wrapper for GitFlic API

> #### 📃Origin api docs: [<u>gitflic.ru/help/api/intro</u>](https://gitflic.ru/help/api/intro)
>
> # 👩‍💻 See on: **[npm](https://www.npmjs.com/package/gitflic-api)**, **[gitflic.ru](https://gitflic.ru/project/yorkblansh/gitflic-api)**, **[js.gitflic-api.tk](https://js.gitflic-api.tk)**

---

## Contents

- [Installing](#installing)
- [Using](#using)
     - [GitFlic Instance](#gitflic-instance)
     - [`.get().by()`](#getby)
- [Roadmap](#roadmap)

## Installing

**npm**

```bash
npm i gitflic-api
```

**yarn**

```bash
yarn add gitflic-api
```

## API

### GitFlic Instance

```ts
import { GitFlic } from 'gitflic-api'

// Creating new instance of Gitflic wrapper:
const gfa = new GitFlic({
	gitflic_api_url: 'https://api.gitflic.ru',
	gitflic_token: '7efee2d6-04ef-4dcb-aad2-ba864598e41a'
}).API
```

\*for [self-hosted](https://gitflic.ru/help/api/intro) use `'localhost:8080/rest-api'` at `gitflic_api_url`

## `.get().by()`

### project

```ts
const params = {
	userAlias: 'yorkblansh',
	projectAlias: 'gitflic-api'
}

gfa.project
	.get('singleProject')
	.by({ params })
	.then((response) => console.log(response.data))
```

### project list

```ts
gfa.project
	.get('myProjects')
	.by({}) // you can set empty props if fetching list
	.then((response) => console.log(response.data))
```

#### Or set <u>**Pagination**</u> options tou return list:

```ts
const params = {
	page: 20 // <- page number
	size: 5 // <- number of objects, displayed on page
}

gfa.project
	.get('myProjects')
	.by({ params })
	.then((response) => console.log(response.data))
```

#### \*argument enums for `get` method:

<details><summary><code>project</code></summary>

```ts
gfa.project.get(/* put one the types below */)...

'allProjects' [] // return project list, can be refined with params
'myProjects' [] // return project list of current user
'sharedProjects' [] // return shared project list for current user
'singleProject' // return single project, have to be specified by user and project aliases
```

</details>

<details><summary><code>user</code></summary>

```ts
gfa.user.get(/* put one the types below */)...

'allUsers' [] // return user list, can be refined with params
'currentUser' // return current user
'singleUser' // ❗️not released yet
```

</details>

<details><summary><code>team</code></summary>

```ts
gfa.team.get(/* put one the types below */)...

'allTeams' [] // return project list, can be refined with params
'myTeams' [] // return project list of current user
'sharedTeams' [] // return shared project list for current user
'singleTeam' // return single project, have to be specified by user and project aliases
```

</details>

<details><summary><code>company</code></summary>

```ts
gfa.company.get(/* put one the types below */)...

'allCompanies' [] // return project list, can be refined with params
'myCompanies' [] // return project list of current user
'singleCompany' // return single project, have to be specified by user and project aliases
```

</details>

[<u>read more about argument enums for get method</u>]()

---

> Get method is also relevant for:
> \
> Project,
> \
> User,
> \
> Team,
> \
> Company,
> \
> Release,
> \
> Commit APIs
> \
> [<u>see more examples at extended docs</u>]()

## Roadmap

- 🕸️API
     - [x] Project
     - [ ] User (in progress: `singleUser`)
     - [ ] Team (in progress: `Teamlist`)
     - [x] Company
     - [x] Commit
     - [ ] Release (need e2e tests)
     - [ ] MergeRequest (need e2e tests)
     - [ ] IssueDiscussion (need e2e tests)
