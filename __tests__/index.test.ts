import { GitFlic } from '../lib/GitFlic'
import { expectTypeOf } from 'expect-type'
import { GitFlicCredentials } from '../src/interfaces/gitflic.credentials.interface'

test('Should check GitFlic class', () => {
	expectTypeOf(GitFlic).constructorParameters.toEqualTypeOf<[GitFlicCredentials]>()
	expectTypeOf(GitFlic).instance.toHaveProperty('API')
	expectTypeOf(GitFlic).instance.toHaveProperty('projectQuery')
})
