import { EntitieList, GitFlicCredentials } from 'interfaces'
import { MergeRequestModel } from 'interfaces/merge-request/merge.request.model.interface'
import { MergeRequestQueryProps } from 'interfaces/merge-request/merge.request.query.props.interface'
import { MergeRequestPerformQueryProps } from 'interfaces/merge-request/query.props/merge.request.perform.query.props.interface'
import { Params } from 'interfaces/params.generic.interface'
import { curriedFetch } from 'utils/fetch/curriedFetch'
import { mergeRequestPath } from 'utils/path/mergeRequestPath'

export class MergeRequestServices {
	private fetch: ReturnType<typeof curriedFetch>

	constructor(private gc: GitFlicCredentials) {
		this.fetch = curriedFetch(this.gc)
	}

	getList(props: Params<MergeRequestQueryProps>) {
		const options = this.options({ ...props, queryType: 'list' })
		return this.fetch<EntitieList<'mergeRequestModelList'>>({ method: 'get', options })
	}

	getSingle(props: Params<MergeRequestQueryProps>) {
		const options = this.options({ ...props, queryType: 'single' })
		return this.fetch<MergeRequestModel>({ method: 'get', options })
	}

	modify(props: Params<MergeRequestQueryProps & MergeRequestPerformQueryProps>) {
		const options = this.options({ ...props, queryType: 'modify' })
		return this.fetch<MergeRequestModel>({ method: 'put', options })
	}

	create(props: Params<MergeRequestQueryProps & MergeRequestPerformQueryProps>) {
		const options = this.options({ ...props, queryType: 'create' })
		return this.fetch<MergeRequestModel>({ method: 'post', options })
	}

	perform(props: Params<MergeRequestQueryProps & MergeRequestPerformQueryProps>) {
		const options = this.options({ ...props, queryType: 'perform' })
		return this.fetch<MergeRequestModel>({ method: 'post', options })
	}

	close(props: Params<MergeRequestQueryProps & MergeRequestPerformQueryProps>) {
		const options = this.options({ ...props, queryType: 'close' })
		return this.fetch<MergeRequestModel>({ method: 'post', options })
	}

	cancel(props: Params<MergeRequestQueryProps & MergeRequestPerformQueryProps>) {
		const options = this.options({ ...props, queryType: 'cancel' })
		return this.fetch<MergeRequestModel>({ method: 'post', options })
	}

	private options(
		props: Params<MergeRequestQueryProps> & {
			queryType: keyof ReturnType<typeof mergeRequestPath>
		}
	) {
		const path = mergeRequestPath(props)[props.queryType]()
		return { params: { ...props.params, path } }
	}
}
