import { Pagination } from 'interfaces'
import { MergeRequestQueryProps } from 'interfaces/merge-request/merge.request.query.props.interface'
import { Params } from 'interfaces/params.generic.interface'

export interface MergeRequestQueryOptions {
	releaseList: Params<MergeRequestQueryProps>
	singleMergeRequest: Params<MergeRequestQueryProps>
}

export type MergeRequestQueryOptionsGroup<Name extends keyof MergeRequestQueryOptions> =
	MergeRequestQueryOptions[Name]

type MergeRequestListGroup = 'mrList'

type SingleMergeRequestGroup = 'singleMR'
// | 'singleMergeRequestFile'

export interface MergeRequestQueryGroup {
	list: MergeRequestListGroup
	single: SingleMergeRequestGroup
}

export type MRQueryType = MergeRequestListGroup | SingleMergeRequestGroup
