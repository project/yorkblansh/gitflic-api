import { AxiosResponse } from 'axios-from-npm'
import { EntitieList } from 'interfaces'
import { MergeRequestModel } from 'interfaces/merge-request/merge.request.model.interface'
import { MergeRequestQueryProps } from 'interfaces/merge-request/merge.request.query.props.interface'
import { Params } from 'interfaces/params.generic.interface'
import { MergeRequestServices } from './merge.request.services'
import { MergeRequestQueryGroup, MRQueryType } from './typings/merge.request.api.typings'

export class MergeRequestQueryChain {
	constructor(private S: MergeRequestServices) {
		this.modify = this.S.modify
		this.create = this.S.create
		this.cancel = this.S.cancel
		this.close = this.S.close
	}
	modify
	create
	cancel
	close

	get<
		RQT extends MRQueryType,
		ReturnType = RQT extends MergeRequestQueryGroup['list']
			? EntitieList<'mergeRequestModelList'>
			: MergeRequestModel,
		// RQO = RQT extends 'releaseList'
		// 	? MRQueryOptions['releaseList']
		// 	: MRQueryOptions['singleMR'],
		PromiseReturnType = Promise<AxiosResponse<ReturnType, any>>
	>(releaseQueryType: RQT) {
		return {
			by: (options: Params<MergeRequestQueryProps>) =>
				this.SERVICE_MAP(options)[releaseQueryType] as unknown as PromiseReturnType
		}
	}

	private SERVICE_MAP = (options: Params<MergeRequestQueryProps>) => ({
		singleMR: this.S.getSingle(options),
		mrList: this.S.getList(options)
	})
}
