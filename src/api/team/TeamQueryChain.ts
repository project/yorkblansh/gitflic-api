import { TeamServices } from 'api/team/team.services'
import { Team, EntitieList } from 'interfaces'
import {
	TeamQueryType,
	TeamQueryGroup,
	TeamQueryOptionsGroup
} from 'api/team/typings/team.api.typings'
import { AxiosResponse } from 'axios-from-npm'

export class TeamQueryChain {
	constructor(private _TEAM_QUERY_MAP: TeamServices['TEAM_QUERY_MAP']) {}

	public get = <
		PQT extends TeamQueryType,
		TeamReturnType = PQT extends TeamQueryGroup['teamList']
			? EntitieList<'teamList'>
			: Team,
		PQO = PQT extends TeamQueryGroup['teamList']
			? TeamQueryOptionsGroup<'teamList'>
			: TeamQueryOptionsGroup<'singleTeam'>,
		PromiseReturnType = Promise<AxiosResponse<TeamReturnType, any>>
	>(
		teamQueryType: PQT
	) => ({
		by: (options: PQO) =>
			this._TEAM_QUERY_MAP({
				options,
				teamQueryType
			})[teamQueryType]() as unknown as PromiseReturnType
	})
}
