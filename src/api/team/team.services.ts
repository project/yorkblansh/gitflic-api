import { EntitieList } from 'interfaces/response.frame.interface'
import { GitFlicCredentials, Team } from 'interfaces'
import { TeamSpecification, TEAM_QUERY_MAP_props } from 'api/team/typings/team.api.typings'
import { TEAM_MAPS } from 'api/team/TEAM_MAPS'
import { curriedFetch } from 'utils/fetch/curriedFetch'
import { teamPath } from 'utils/path/teamPath'

export class TeamServices {
	private _TEAM_PATH_MAP
	public TEAM_QUERY_MAP
	private _fetch: ReturnType<typeof curriedFetch>

	constructor(private gc: GitFlicCredentials) {
		const tmaps = new TEAM_MAPS()
		this._TEAM_PATH_MAP = tmaps.TEAM_PATH_MAP
		this.TEAM_QUERY_MAP = (tmp: TEAM_QUERY_MAP_props) => tmaps.TEAM_QUERY_MAP(tmp)(this)
		this._fetch = curriedFetch(this.gc)
	}

	public getTeamList({ options, teamQueryType }: TEAM_QUERY_MAP_props) {
		const path = this._TEAM_PATH_MAP()[teamQueryType]
		// [ ] Need refactor | нужно убрать мапы
		const _options = { params: { path: teamPath(path), ...options?.params } }
		return this._fetch<EntitieList<'teamList'>>({ method: 'get', options: _options })
	}

	public getSingleTeam({ options, teamQueryType }: TEAM_QUERY_MAP_props) {
		console.dir(options)
		const {
			params: { teamAlias }
		} = options as { params: TeamSpecification }
		// [ ] Need refactor | нужно убрать мапы
		const path = this._TEAM_PATH_MAP(teamPath(`${teamAlias}`))[teamQueryType]
		console.dir(path)
		const _options = { params: { path, ...options?.params } }
		return this._fetch<Team>({ method: 'get', options: _options })
	}
}
