import { Pagination } from 'interfaces'

export type TeamSpecification = {
	teamAlias: string
}

export interface TeamQueryOptions {
	teamList?: Pagination
	singleTeam: { params: TeamSpecification }
}

export type TeamQueryOptionsGroup<Name extends keyof TeamQueryOptions> = TeamQueryOptions[Name]

type TeamListGroup = 'allTeams' | 'myTeams' | 'sharedTeams'

type SingleTeamGroup = 'singleTeam'

export interface TeamQueryGroup {
	teamList: TeamListGroup
	singleTeam: SingleTeamGroup
}

export type TeamQueryType = TeamListGroup | SingleTeamGroup

export interface TEAM_QUERY_MAP_props {
	options: TeamQueryOptions['teamList'] | TeamQueryOptions['singleTeam']
	teamQueryType: TeamQueryType
}
