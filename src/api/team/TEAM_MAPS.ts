import { TeamQueryType, TEAM_QUERY_MAP_props } from 'api/team/typings/team.api.typings'
import { TeamServices } from 'api/team/team.services'

export class TEAM_MAPS {
	public TEAM_PATH_MAP = (customPath?: string) =>
		({
			allTeams: '',
			myTeams: 'my',
			sharedTeams: 'shared',
			singleTeam: customPath ? customPath : 'my'
		} as { [each in TeamQueryType]: string })

	public TEAM_QUERY_MAP = (TQMp: TEAM_QUERY_MAP_props) => (TS: TeamServices) =>
		({
			singleTeam: () => TS.getSingleTeam(TQMp),
			allTeams: () => TS.getTeamList(TQMp),
			myTeams: () => TS.getTeamList(TQMp),
			sharedTeams: () => TS.getTeamList(TQMp)
		} as { [each in TeamQueryType]: () => unknown })
}
