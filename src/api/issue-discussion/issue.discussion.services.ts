import {
	GitFlicCredentials,
	IssueDiscussion,
	IssueDiscussionQueryProps,
	EntitieList
} from 'interfaces'
import { curriedFetch } from 'utils/fetch/curriedFetch'
import { issueDiscussionPath } from 'utils/path/issueDiscussionPath'

export class IssueDiscussionServices {
	private _fetch: ReturnType<typeof curriedFetch>

	constructor(private gc: GitFlicCredentials) {
		this._fetch = curriedFetch(this.gc)
	}

	//[ ] Указать тип options
	public getIssueDiscussionList(idqp: IssueDiscussionQueryProps, options?: any) {
		const path = issueDiscussionPath('get', idqp)
		const _options = { params: { path, ...options?.params } }
		return this._fetch<EntitieList<'issueDiscussionList'>>({
			method: 'get',
			options: _options
		})
	}

	public createIssueDiscussion(idqp: IssueDiscussionQueryProps, note: string) {
		// [ ] нужно отладить, протестить, не факт что работает
		const path = issueDiscussionPath('create', idqp)
		const _options = { params: { path, ...idqp, note } }
		return this._fetch<IssueDiscussion>({ method: 'post', options: _options })
	}
}
