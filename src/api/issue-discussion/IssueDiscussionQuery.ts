import { Pagination, IssueDiscussionQueryProps } from 'interfaces'
import { IssueDiscussionServices } from 'api/issue-discussion/issue.discussion.services'

export class IssueDiscussionQuery {
	constructor(private S: IssueDiscussionServices) {}

	public getIssueDiscussionList = (idqp: IssueDiscussionQueryProps, options?: Pagination) =>
		this.S.getIssueDiscussionList(idqp, options)

	public createIssueDiscussion = (idqp: IssueDiscussionQueryProps, note: string) =>
		this.S.createIssueDiscussion(idqp, note)
}
