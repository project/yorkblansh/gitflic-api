import { EntitieList } from 'interfaces/response.frame.interface'
import { GitFlicCredentials } from 'interfaces'
import { companyPath } from 'utils/path/companyPath'
import { Company } from 'interfaces/company/company.interface'
import {
	CompanySpecification,
	COMPANY_QUERY_MAP_props
} from 'api/company/typings/company.api.typings'
import { COMPANY_MAPS } from 'api/company/COMPANY_MAPS'
import { curriedFetch } from 'utils/fetch/curriedFetch'

export class CompanyServices {
	private _COMPANY_PATH_MAP
	public COMPANY_QUERY_MAP
	private _fetch: ReturnType<typeof curriedFetch>

	constructor(private gc: GitFlicCredentials) {
		const cmaps = new COMPANY_MAPS()
		this._COMPANY_PATH_MAP = cmaps.COMPANY_PATH_MAP
		this.COMPANY_QUERY_MAP = (CQMp: COMPANY_QUERY_MAP_props) =>
			cmaps.COMPANY_QUERY_MAP(CQMp)(this)
		this._fetch = curriedFetch(this.gc)
	}

	public getCompanyList({ options, companyQueryType }: COMPANY_QUERY_MAP_props) {
		const path = this._COMPANY_PATH_MAP()[companyQueryType]
		const _options = { params: { path: companyPath(path), ...options?.params } }
		return this._fetch<EntitieList<'companyList'>>({ method: 'get', options: _options })
	}

	public getSingleCompany({ options, companyQueryType }: COMPANY_QUERY_MAP_props) {
		console.dir(options)
		const {
			params: { companyAlias }
		} = options as { params: CompanySpecification }
		const path = this._COMPANY_PATH_MAP(companyPath(`${companyAlias}`))[companyQueryType]
		console.dir(path)
		const _options = { params: { path, ...options?.params } }
		return this._fetch<Company>({ method: 'get', options: _options })
	}
}
