import { CompanyServices } from 'api/company/company.services'
import { EntitieList } from 'interfaces'
import {
	CompanyQueryType,
	CompanyQueryGroup,
	CompanyQueryOptionsGroup
} from 'api/company/typings/company.api.typings'
import { AxiosResponse } from 'axios-from-npm'
import { Company } from 'interfaces/company/company.interface'

export class CompanyQueryChain {
	constructor(private _COMPANY_QUERY_MAP: CompanyServices['COMPANY_QUERY_MAP']) {}

	public get = <
		PQT extends CompanyQueryType,
		CompanyReturnType = PQT extends CompanyQueryGroup['companyList']
			? EntitieList<'companyList'>
			: Company,
		PQO = PQT extends CompanyQueryGroup['companyList']
			? CompanyQueryOptionsGroup<'companyList'>
			: CompanyQueryOptionsGroup<'singleCompany'>,
		PromiseReturnType = Promise<AxiosResponse<CompanyReturnType, any>>
	>(
		companyQueryType: PQT
	) => ({
		by: (options: PQO) =>
			this._COMPANY_QUERY_MAP({
				options,
				companyQueryType
			})[companyQueryType]() as unknown as PromiseReturnType
	})
}
