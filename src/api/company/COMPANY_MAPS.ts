import { CompanyQueryType, COMPANY_QUERY_MAP_props } from 'api/company/typings/company.api.typings'
import { CompanyServices } from 'api/company/company.services'

export class COMPANY_MAPS {
	public COMPANY_PATH_MAP = (customPath?: string) =>
		({
			allCompanies: '',
			myCompanies: 'my',
			singleCompany: customPath ? customPath : 'my'
		} as { [each in CompanyQueryType]: string })

	public COMPANY_QUERY_MAP = (PTMp: COMPANY_QUERY_MAP_props) => (PAS: CompanyServices) =>
		({
			singleCompany: () => PAS.getSingleCompany(PTMp),
			allCompanies: () => PAS.getCompanyList(PTMp),
			myCompanies: () => PAS.getCompanyList(PTMp)
		} as { [each in CompanyQueryType]: () => unknown })
}
