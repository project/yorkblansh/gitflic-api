import { Pagination } from 'interfaces'

export type CompanySpecification = {
	companyAlias: string
}

export interface CompanyQueryOptions {
	companyList?: Pagination
	singleCompany: { params: CompanySpecification }
}

export type CompanyQueryOptionsGroup<Name extends keyof CompanyQueryOptions> =
	CompanyQueryOptions[Name]

type CompanyListGroup = 'allCompanies' | 'myCompanies'

type SingleCompanyGroup = 'singleCompany'

export interface CompanyQueryGroup {
	companyList: CompanyListGroup
	singleCompany: SingleCompanyGroup
}

export type CompanyQueryType = CompanyListGroup | SingleCompanyGroup

export interface COMPANY_QUERY_MAP_props {
	options: CompanyQueryOptions['companyList'] | CompanyQueryOptions['singleCompany']
	companyQueryType: CompanyQueryType
}
