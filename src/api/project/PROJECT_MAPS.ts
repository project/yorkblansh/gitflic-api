import { ProjectQueryType, PROJECT_QUERY_MAP_props } from 'api/project/typings/project.api.typings'
import { ProjectServices } from 'api/project/project.services'

export class PROJECT_MAPS {
	public PROJECT_PATH_MAP = (customPath?: string) =>
		({
			allProjects: '',
			myProjects: 'my',
			sharedProjects: 'shared',
			singleProject: customPath ? customPath : 'my'
		} as { [each in ProjectQueryType]: string })

	public PROJECT_QUERY_MAP = (PTMp: PROJECT_QUERY_MAP_props) => (PAS: ProjectServices) =>
		({
			singleProject: () => PAS.getSingleProject(PTMp),
			allProjects: () => PAS.getProjectList(PTMp),
			myProjects: () => PAS.getProjectList(PTMp),
			sharedProjects: () => PAS.getProjectList(PTMp)
		} as { [each in ProjectQueryType]: () => unknown })
}
