import { ProjectServices } from 'api/project/project.services'
import { Project, EntitieList } from 'interfaces'
import {
	ProjectQueryType,
	ProjectQueryGroup,
	ProjectQueryOptionsGroup
} from 'api/project/typings/project.api.typings'
import { AxiosResponse } from 'axios-from-npm'

export class ProjectQueryChain {
	constructor(private _PROJECT_QUERY_MAP: ProjectServices['PROJECT_QUERY_MAP']) {}

	public get = <
		PQT extends ProjectQueryType,
		ProjectReturnType = PQT extends ProjectQueryGroup['projectList']
			? EntitieList<'projectList'>
			: Project,
		PQO = PQT extends ProjectQueryGroup['projectList']
			? ProjectQueryOptionsGroup<'projectList'>
			: ProjectQueryOptionsGroup<'singleProject'>,
		PromiseReturnType = Promise<AxiosResponse<ProjectReturnType, any>>
	>(
		projectQueryType: PQT
	) => ({
		by: (options: PQO) =>
			this._PROJECT_QUERY_MAP({
				options,
				projectQueryType
			})[projectQueryType]() as unknown as PromiseReturnType
	})
}
