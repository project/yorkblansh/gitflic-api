import { Project } from 'interfaces/project/project.interface'
import { EntitieList } from 'interfaces/response.frame.interface'
import { UserSpecification, PROJECT_QUERY_MAP_props } from 'api/project/typings/project.api.typings'
import { GitFlicCredentials } from 'interfaces'
import { PROJECT_MAPS } from 'api/project'
import { curriedFetch } from 'utils/fetch/curriedFetch'
import { projectPath } from 'utils/path/projectPath'

export class ProjectServices {
	private _PROJECT_PATH_MAP
	public PROJECT_QUERY_MAP
	private _fetch: ReturnType<typeof curriedFetch>

	constructor(private gc: GitFlicCredentials) {
		const pmaps = new PROJECT_MAPS()
		this._PROJECT_PATH_MAP = pmaps.PROJECT_PATH_MAP
		this.PROJECT_QUERY_MAP = (tmp: PROJECT_QUERY_MAP_props) =>
			pmaps.PROJECT_QUERY_MAP(tmp)(this)
		this._fetch = curriedFetch(this.gc)
	}

	public getProjectList({ options, projectQueryType }: PROJECT_QUERY_MAP_props) {
		const path = this._PROJECT_PATH_MAP()[projectQueryType]
		const _options = { params: { path: projectPath(path), ...options?.params } }
		// console.dir(_options)
		return this._fetch<EntitieList<'projectList'>>({ method: 'get', options: _options })
	}

	public getSingleProject({ options, projectQueryType }: PROJECT_QUERY_MAP_props) {
		console.dir(options)
		const {
			params: { userAlias, projectAlias }
		} = options as { params: UserSpecification }
		const path = this._PROJECT_PATH_MAP(projectPath(`${userAlias}/${projectAlias}`))[
			projectQueryType
		]
		console.dir(path)
		const _options = { params: { path, ...options?.params } }
		return this._fetch<Project>({ method: 'get', options: _options })
	}
}
