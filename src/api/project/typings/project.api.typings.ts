import { Pagination } from 'interfaces'

export type UserSpecification = {
	userAlias: string
	projectAlias: string
}

export interface ProjectQueryOptions {
	projectList?: Pagination
	singleProject: { params: UserSpecification }
}

export type ProjectQueryOptionsGroup<Name extends keyof ProjectQueryOptions> =
	ProjectQueryOptions[Name]

type ProjectListGroup = 'allProjects' | 'myProjects' | 'sharedProjects'

type SingleProjectGroup = 'singleProject'

export interface ProjectQueryGroup {
	projectList: ProjectListGroup
	singleProject: SingleProjectGroup
}

export type ProjectQueryType = ProjectListGroup | SingleProjectGroup

export interface PROJECT_QUERY_MAP_props {
	options: ProjectQueryOptions['projectList'] | ProjectQueryOptions['singleProject']
	projectQueryType: ProjectQueryType
}
