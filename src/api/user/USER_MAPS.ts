import { UserQueryType, USER_QUERY_MAP_props } from 'api/user/typings/user.api.typings'
import { UserServices } from './user.services'

export class USER_MAPS {
	public USER_PATH_MAP = (customPath?: string): { [target: string]: string } =>
		({
			allUsers: '',
			currentUser: 'me',
			singleUser: customPath ? customPath : 'me'
		} as { [each in UserQueryType]: string })

	public USER_QUERY_MAP = (UQMp: USER_QUERY_MAP_props) => (US: UserServices) =>
		({
			allUsers: () => US.getUserList(UQMp),
			currentUser: () => US.getCurrentUser(UQMp)
			//  singleUser: () => US.getSingleUser(UQMp)
		} as {
			[each in UserQueryType]: () => unknown // [ ] заменить unknown
		})
}
