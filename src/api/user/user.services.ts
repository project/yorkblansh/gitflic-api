import { USER_MAPS } from 'api/user/USER_MAPS'
import { GitFlicCredentials, EntitieList, User } from 'interfaces'
import { USER_QUERY_MAP_props } from 'api/user/typings/user.api.typings'
import { curriedFetch } from 'utils/fetch/curriedFetch'
import { userPath } from 'utils/path/userPath'

export class UserServices {
	private _USER_PATH_MAP
	public USER_QUERY_MAP
	private _fetch: ReturnType<typeof curriedFetch>

	constructor(private gc: GitFlicCredentials) {
		const umaps = new USER_MAPS()
		this._USER_PATH_MAP = umaps.USER_PATH_MAP
		this.USER_QUERY_MAP = (uqmp: USER_QUERY_MAP_props) => umaps.USER_QUERY_MAP(uqmp)(this)
		this._fetch = curriedFetch(gc)
	}

	//[x] getUserList()
	public getUserList({ options, userQueryType }: USER_QUERY_MAP_props) {
		const path = this._USER_PATH_MAP()[userQueryType]
		return this._fetch<EntitieList<'userList'>>({
			method: 'get',
			options: {
				params: { path: userPath(path ? path : 'me'), ...options?.params }
			}
		})
	}

	//[x] getCurrentUser()
	public getCurrentUser({ options, userQueryType }: USER_QUERY_MAP_props) {
		const isCurrentUser = userQueryType === 'currentUser'
		const path = isCurrentUser && this._USER_PATH_MAP()[userQueryType]
		return this._fetch<User>({
			method: 'get',
			options: {
				params: { path: userPath(path ? path : 'me'), ...options?.params }
			}
		})
	}

	//[ ] getSpecifiedUser()
	public getSpecifiedUser() {}

	// public getSingleUser({ options, userQueryType }: USER_QUERY_MAP_props) {
	// 	// const { userAlias } = options as unknown as UserQueryOptions['specifiedUser']

	// 	const isCurrentUser = userQueryType === 'currentUser'
	// 	// const isSpecifiedUser = userQueryType === 'singleUser'

	// 	const getPath = (arg: string) => this._USER_PATH_MAP(arg)[userQueryType]

	// 	let path: string
	// 	// const uuid = this.getUserUUID(userAlias)

	// 	if (isCurrentUser) {
	// 		path = getPath('me')
	// 		return AxiosQuery<User>(this.gc, userPath(path), options)
	// 	} else return AxiosQuery<User>(this.gc, userPath('me'), options)

	// 	// else if (isSpecifiedUser) path = getPath(uuid)
	// 	// const path = this._USER_PATH_MAP(uuid ? uuid : 'me')[userQueryType]
	// 	// console.dir(userPath(path))
	// }

	// private async getUserUUID(userAlias: string) {
	// 	const response = await this.getUserList({ options: {}, userQueryType: 'allUsers' })
	// 	console.dir(response.data._embedded.userList)
	// 	const uuid = response.data._embedded.userList
	// 		.map((user) => user.username === userAlias && user.id)
	// 		.filter(Boolean) as string[]
	// 	return { uuid, userAlias }
	// }
}
