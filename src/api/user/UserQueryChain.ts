import { AxiosResponse } from 'axios-from-npm'
import { EntitieList, User } from 'interfaces'
import {
	UserQueryGroup,
	UserQueryOptionsGroup,
	UserQueryType
} from 'api/user/typings/user.api.typings'
import { UserServices } from './user.services'

export class UserQueryChain {
	constructor(private _USER_AUERY_MAP: UserServices['USER_QUERY_MAP']) {}

	public get = <
		UQT extends UserQueryType,
		UserReturnType = UQT extends UserQueryGroup['userList']
			? EntitieList<'userList'>
			: User,
		UserQueryOption = UQT extends UserQueryGroup['userList']
			? UserQueryOptionsGroup<'userList'>
			: UQT extends UserQueryGroup['currentUser']
			? UserQueryOptionsGroup<'currentUser'>
			: UserQueryOptionsGroup<'specifiedUser'>,
		PromiseReturnType = Promise<AxiosResponse<UserReturnType, any>>
	>(
		userQueryType: UQT
	) => ({
		by: (options: UserQueryOption) =>
			this._USER_AUERY_MAP({ options, userQueryType })[
				userQueryType
			]() as unknown as PromiseReturnType
	})
}
