import { GitFlicCredentials } from 'interfaces'
import { CommitServices } from './commit/commit.services'
import { CommitQueryChain } from './commit/CommitQueryChain'
import { CompanyServices } from './company/company.services'
import { CompanyQueryChain } from './company/CompanyQueryChain'
import { IssueDiscussionServices } from './issue-discussion/issue.discussion.services'
import { IssueDiscussionQuery } from './issue-discussion/IssueDiscussionQuery'
import { MergeRequestServices } from './merge-request/merge.request.services'
import { MergeRequestQueryChain } from './merge-request/MergeRequestQueryChain'
import { ProjectQueryChain } from './project'
import { ProjectServices } from './project/project.services'
import { ReleaseServices } from './release/release.services'
import { ReleaseQueryChain } from './release/ReleaseQueryChain'
import { TeamServices } from './team/team.services'
import { TeamQueryChain } from './team/TeamQueryChain'
import { UserQueryChain } from './user'
import { UserServices } from './user/user.services'

export class API {
	user
	project
	team
	company
	issueDiscussion
	commit
	release
	mergeRequest

	constructor(private gc: GitFlicCredentials) {
		this.project = new ProjectQueryChain(new ProjectServices(this.gc).PROJECT_QUERY_MAP)
		this.user = new UserQueryChain(new UserServices(this.gc).USER_QUERY_MAP)
		this.team = new TeamQueryChain(new TeamServices(this.gc).TEAM_QUERY_MAP)
		this.company = new CompanyQueryChain(new CompanyServices(this.gc).COMPANY_QUERY_MAP)
		this.issueDiscussion = new IssueDiscussionQuery(new IssueDiscussionServices(this.gc))
		this.commit = new CommitQueryChain(new CommitServices(this.gc).COMMIT_QUERY_MAP)
		this.release = new ReleaseQueryChain(new ReleaseServices(this.gc))
		this.mergeRequest = new MergeRequestQueryChain(new MergeRequestServices(this.gc))
	}
}
