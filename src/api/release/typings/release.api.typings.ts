import { Pagination } from 'interfaces'
import { Params } from 'interfaces/params.generic.interface'
import { ReleaseQueryProps } from 'interfaces/release/release.query.props.interface'

export interface ReleaseQueryOptions {
	releaseList: Params<ReleaseQueryProps>
	singleRelease: Params<ReleaseQueryProps>
}

export type ReleaseQueryOptionsGroup<Name extends keyof ReleaseQueryOptions> =
	ReleaseQueryOptions[Name]

type ReleaseListGroup = 'releaseList'

type SingleReleaseGroup = 'singleRelease'
// | 'singleReleaseFile'

export interface ReleaseQueryGroup {
	releaseList: ReleaseListGroup
	singleRelease: SingleReleaseGroup
}

export type ReleaseQueryType = ReleaseListGroup | SingleReleaseGroup
