import { EntitieList } from 'interfaces/response.frame.interface'
import { GitFlicCredentials } from 'interfaces'
import { curriedFetch } from 'utils/fetch/curriedFetch'
import { Release } from 'interfaces/release/release.interface'
import { releasePath } from 'utils/path/releasePath'
import { Params } from 'interfaces/params.generic.interface'
import { ReleasePerformQueryProps } from 'interfaces/release/release.perform.query.props.interface'
import { ReleaseQueryProps } from 'interfaces/release/release.query.props.interface'

export class ReleaseServices {
	private fetch: ReturnType<typeof curriedFetch>

	constructor(private gc: GitFlicCredentials) {
		this.fetch = curriedFetch(this.gc)
	}

	public getReleaseList(props: Params<ReleaseQueryProps>) {
		const path = releasePath(props).releaseList()
		const options = this._releaseOptions({ ...props }, path)

		return this.fetch<EntitieList<'releaseTagModelList'>>({ method: 'get', options })
	}

	public getSingleRelease(props: Params<ReleaseQueryProps>) {
		const path = releasePath(props).singleRelease()
		const options = this._releaseOptions({ ...props }, path)

		return this.fetch<Release>({ method: 'get', options })
	}

	public modifyRelease(props: Params<ReleaseQueryProps & ReleasePerformQueryProps>) {
		const path = releasePath(props).singleRelease()
		const options = this._releaseOptions({ ...props }, path)

		return this.fetch<Release>({ method: 'put', options })
	}

	public createRelease(props: Params<ReleaseQueryProps & ReleasePerformQueryProps>) {
		const path = releasePath(props).singleRelease()
		const options = this._releaseOptions({ ...props }, path)

		return this.fetch<Release>({ method: 'post', options })
	}

	public deleteRelease(props: Params<ReleaseQueryProps>) {
		const path = releasePath(props).singleRelease()
		const options = this._releaseOptions({ ...props }, path)

		return this.fetch<Release>({ method: 'delete', options })
	}

	public getSingleReleaseFile() {
		// [ ] getSingleReleaseFile()
	}

	public uploadReleaseFileList() {
		// [ ] uploadReleaseFileList()
	}

	public deleteReleaseFileList() {
		// [ ] deleteReleaseFileList()
	}

	private _releaseOptions(
		props: // [ ] найти лучший тип
		Params<{}>,
		path: string
	) {
		return { params: { ...props.params, path } }
	}
}
