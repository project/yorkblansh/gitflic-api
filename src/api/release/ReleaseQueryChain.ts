import { AxiosResponse } from 'axios-from-npm'
import { EntitieList } from 'interfaces'
import { Params } from 'interfaces/params.generic.interface'
import { Release } from 'interfaces/release/release.interface'
import { ReleaseQueryProps } from 'interfaces/release/release.query.props.interface'
import { ReleaseServices } from './release.services'
import {
	ReleaseQueryGroup,
	ReleaseQueryOptions,
	ReleaseQueryType
} from './typings/release.api.typings'

export class ReleaseQueryChain {
	constructor(private S: ReleaseServices) {
		this.modifyRelease = this.S.modifyRelease
		this.createRelease = this.S.createRelease
		this.deleteRelease = this.S.deleteRelease
	}
	modifyRelease
	createRelease
	deleteRelease

	get = <
		RQT extends ReleaseQueryType,
		ReleaseReturnType = RQT extends ReleaseQueryGroup['releaseList']
			? EntitieList<'releaseTagModelList'>
			: Release,
		// RQO = RQT extends 'releaseList'
		// 	? ReleaseQueryOptions['releaseList']
		// 	: ReleaseQueryOptions['singleRelease'],
		PromiseReturnType = Promise<AxiosResponse<ReleaseReturnType, any>>
	>(
		releaseQueryType: RQT
	) => ({
		by: (options: Params<ReleaseQueryProps>) =>
			this.getRelease_MAP(options)[releaseQueryType] as unknown as PromiseReturnType
	})

	private getRelease_MAP = (options: Params<ReleaseQueryProps>) => ({
		singleRelease: this.S.getSingleRelease(options),
		releaseList: this.S.getReleaseList(options)
	})
}
