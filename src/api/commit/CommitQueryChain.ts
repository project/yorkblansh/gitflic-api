import { AxiosResponse } from 'axios-from-npm'
import { Commit } from 'interfaces/commit/commit.interface'
import { CommitQueryProps } from 'interfaces/commit/commit.query.props.interface'
import { FileAlter } from 'interfaces/commit/file/file.alter.interface'
import { CommitServices } from './commit.services'
import { CommitQueryGroup, CommitQueryType } from './typings/commit.api.typings'

export class CommitQueryChain {
	constructor(private _COMMIT_QUERY_MAP: CommitServices['COMMIT_QUERY_MAP']) {}

	public get = <
		CQT extends CommitQueryType,
		ReturnType = CQT extends CommitQueryGroup['commit']
			? Commit
			: CQT extends CommitQueryGroup['fileAlter']
			? FileAlter
			: FileAlter[],
		PromiseReturnType = Promise<AxiosResponse<ReturnType, any>>
	>(
		commitQueryType: CQT
	) => ({
		by: (options: CommitQueryProps) =>
			this._COMMIT_QUERY_MAP(options)[commitQueryType]() as unknown as PromiseReturnType
	})
}
