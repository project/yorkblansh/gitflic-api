import { AxiosResponse } from 'axios-from-npm'
import { CommitQueryProps } from 'interfaces/commit/commit.query.props.interface'
import { CommitServices } from './commit.services'
import { CommitQueryType } from './typings/commit.api.typings'

export class COMMIT_MAPS {
	// public COMMIT_PATH_MAP = (customPath?: string) =>
	// 	({
	// 		affectedFileList: '',
	// 		commit: 'my',
	// 		fileCommit: customPath ? customPath : 'my',
	// 		affectedFile
	// 	} as { [each in CommitQueryType]: string })

	public COMMIT_QUERY_MAP = (cqt: CommitQueryProps) => (CS: CommitServices) =>
		({
			affectedFile: () => CS.getAffectedFile(cqt),
			affectedFileList: () => CS.getAffectedFileList(cqt),
			commit: () => CS.getCommit(cqt),
			fileCommit: () => CS.getFileCommit(cqt)
		} as { [each in CommitQueryType]: () => Promise<AxiosResponse<any, any>> })
}
