import { CommitQueryProps } from 'interfaces/commit/commit.query.props.interface'

export interface CommitQueryOptions {
	list: CommitQueryProps
	commit: CommitQueryProps
}

export type CommitQueryOptionsGroup<Name extends keyof CommitQueryOptions> =
	CommitQueryOptions[Name]

type FileAlterListGroup = 'affectedFileList'

type SingleCommitGroup = 'commit' | 'fileCommit'

type SingleFileAlter = 'affectedFile'

export interface CommitQueryGroup {
	fileAlterList: FileAlterListGroup
	commit: SingleCommitGroup
	fileAlter: SingleFileAlter
}

export type CommitQueryType = FileAlterListGroup | SingleCommitGroup | SingleFileAlter
