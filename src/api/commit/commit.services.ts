import { GitFlicCredentials } from 'interfaces'
import { Commit } from 'interfaces/commit/commit.interface'
import { CommitQueryProps } from 'interfaces/commit/commit.query.props.interface'
import { FileAlter } from 'interfaces/commit/file/file.alter.interface'
import { curriedFetch } from 'utils/fetch/curriedFetch'
import { commitPath } from 'utils/path/commitPath'
import { COMMIT_MAPS } from './COMMIT_MAPS'

export class CommitServices {
	public COMMIT_QUERY_MAP
	private fetch: ReturnType<typeof curriedFetch>

	constructor(private gc: GitFlicCredentials) {
		const cmaps = new COMMIT_MAPS()
		this.COMMIT_QUERY_MAP = (cqt: CommitQueryProps) => cmaps.COMMIT_QUERY_MAP(cqt)(this)
		this.fetch = curriedFetch(this.gc)
	}

	public getCommit(props: CommitQueryProps) {
		const path = commitPath(props).commit()
		const options = { params: { path, ...props.params } }
		return this.fetch<Commit>({ method: 'get', options })
	}

	public getAffectedFileList(props: CommitQueryProps) {
		const path = commitPath(props).affectedFileList(props.params.filePath)
		const options = { params: { path, ...props.params } }
		return this.fetch<FileAlter[]>({ method: 'get', options })
	}

	public getAffectedFile(props: CommitQueryProps) {
		const path = commitPath(props).affectedFile(props.params.filePath)
		const options = { params: { path, ...props.params } }
		return this.fetch<FileAlter>({ method: 'get', options })
	}

	public getFileCommit(props: CommitQueryProps) {
		const path = commitPath(props).fileCommit(props.params.filePath)
		const options = { params: { path, ...props.params } }
		return this.fetch<Commit>({ method: 'get', options })
	}
}
