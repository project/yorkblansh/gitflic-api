// @no-transform-path
export * from 'axios'
// @no-transform-path
import * as _axios from 'axios'

const axiosInstance = _axios.default
export const axios = axiosInstance.create()
export { _axios }
