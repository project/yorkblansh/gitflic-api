import { User } from 'interfaces/user/user.interface'
import { Author_or_CommitterIdent } from './auhor_committer.ident.interface'

export interface Commit {
	hash: string
	message: string
	shortMessage: string
	createdAt: Date // TODO разобраться что за тип нужен https://gitflic.ru/help/api/commit
	committerIdent: Author_or_CommitterIdent
	authorIdent: Author_or_CommitterIdent
	user: User
}
