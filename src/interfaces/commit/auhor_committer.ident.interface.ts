export interface Author_or_CommitterIdent {
	name: string
	avatar: string
	emailAddress: string
	when: Date // TODO разобраться что за тип нужен https://gitflic.ru/help/api/commit
}
