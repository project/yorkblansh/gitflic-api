type FileLineType = 'separator' | 'line'
type FileLineOP = 'none' | 'remove' | 'add'

interface FileAlterLine {
	id: string
	body: string // "@@ -1 +1,3 @@"
	highLightBody: string // "@ -1 +1,3 @@"
	addLineNumber: null | number
	removeLineNumber: null | number
	op: FileLineOP
	type: FileLineType
}
