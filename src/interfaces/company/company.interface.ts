export interface Company {
	id: string
	alias: string
	title: string
	description: string
	url: string
	contactPhone: null | string
	contactEmail: string
	ownerAlias: string
	avatar: string
	selectorTitle: string
	private: boolean
	selectorId: string
	selectorOwnerAlias: null | string
	selectorAlias: null | string
	selectorColor: null | string
	selectorHash: null | string
}
