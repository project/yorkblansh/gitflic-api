export interface Branch {
	id: string
	title: string
	hash: string
	alias: string
	ownerAlias: null | string
	color: null | string
	hexColor: null | string
	icon: null | string
}
