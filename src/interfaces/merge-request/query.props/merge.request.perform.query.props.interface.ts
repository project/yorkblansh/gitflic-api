export interface MergeRequestPerformQueryProps {
	id: string
	description: string
	title: string
	sourceBranch: { id: string }
	targetBranch: { id: string }
	sourceProject: { id: string }
	targetProject: { id: string }
}
