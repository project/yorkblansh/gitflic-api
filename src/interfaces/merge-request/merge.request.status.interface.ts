export interface MergeRequestStatus {
	id: 'OPENED' | 'CANCELED'
	title: string
	hash: null | string
	alias: null | string
	ownerAlias: null | string
	color: null | string
	hexColor: null | string
	icon: null | string
}
