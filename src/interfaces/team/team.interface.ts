export interface Team {
	id: string
	alias: string
	title: string
	description: string
	ownerAlias: string
	avatar: string
	selectorTitle: string
	private: boolean
	selectorId: string
	selectorOwnerAlias: string | null
	selectorAlias: string | null
	selectorColor: string | null
	selectorHash: string | null
}
