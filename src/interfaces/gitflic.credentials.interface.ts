export interface GitFlicCredentials {
	gitflic_api_url: string
	gitflic_token: string
}
