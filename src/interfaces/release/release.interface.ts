import { AttachmentFile } from './release.attachment.file.interface'

export interface Release {
	id: string
	title: string
	projectId: string
	authorId: string
	description: string
	tagName: string
	commitId: string
	createdAt: Date //TODO разобраться что за тип нужен https://gitflic.ru/help/api/release
	updatedAt: Date
	attachmentFiles: AttachmentFile[]
	preRelease: boolean
}
