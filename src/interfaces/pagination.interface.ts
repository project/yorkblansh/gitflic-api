export interface Pagination {
	params?: {
		page: number
		size: number
	}
}
