export interface IssueDiscussionQueryProps {
	userAlias: string
	projectAlias: string
	localId?: number
}
