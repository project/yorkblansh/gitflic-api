import { User } from 'interfaces/user/user.interface'

export interface IssueDiscussion {
	id: string
	message: string
	author: User
	createdAt: Date //TODO Разобраться что за формат: https://gitflic.ru/help/api/issueNote
	createTimeDifference: string
}
