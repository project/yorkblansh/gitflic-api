import { CommitQueryType } from 'api/commit/typings/commit.api.typings'
import { CommitQueryProps } from 'interfaces/commit/commit.query.props.interface'

export const commitPath = ({ params: { userAlias, projectAlias, commitId } }: CommitQueryProps) => {
	const path = `/project/${userAlias}/${projectAlias}/commit/${commitId}`
	return {
		commit: () => `${path}`,
		affectedFileList: () => `${path}/file`,
		affectedFile: (filePath) => `${path}/diff?filePath=${filePath}`,
		fileCommit: (filePath) => `${path}/for-file?filePath=${filePath}`
	} as {
		[each in CommitQueryType]: (filePath?: CommitQueryProps['params']['filePath']) => string
	}
}
