import { IssueDiscussionQueryProps } from 'interfaces'

export function issueDiscussionPath(
	queryType: 'get' | 'create',
	{ userAlias, projectAlias, localId }: IssueDiscussionQueryProps
) {
	// const isGetMethod = queryType === 'get'
	const isCreateMethod = queryType === 'create'

	return `/project/${userAlias}/${projectAlias}/issue-discussion/${localId}/${
		isCreateMethod ? 'create' : ''
	}`
}
