import { ReleaseQueryType } from 'api/release/typings/release.api.typings'
import { Params } from 'interfaces/params.generic.interface'
import { ReleaseQueryProps } from 'interfaces/release/release.query.props.interface'

export const releasePath = ({
	params: { ownerAlias, projectAlias, releaseUuid, fileName }
}: Params<ReleaseQueryProps>) => {
	const path = `/project/${ownerAlias}/${projectAlias}/release`
	return {
		releaseList: () => `${path}`,
		singleRelease: () => `${path}/${releaseUuid}`,
		singleReleaseFile: () => `${path}/${releaseUuid}/file/${fileName}`
	} as {
		[each in ReleaseQueryType]: (filePath?: string) => string
	}
}
