// import { ReleaseQueryType } from 'api/release/typings/release.api.typings'
// import { Params } from 'interfaces/params.generic.interface'
// import { ReleaseQueryProps } from 'interfaces/release/release.query.props.interface'

import { MergeRequestQueryProps } from 'interfaces/merge-request/merge.request.query.props.interface'
import { Params } from 'interfaces/params.generic.interface'

export function mergeRequestPath({
	params: { userAlias, projectAlias, localId }
}: Params<MergeRequestQueryProps>) {
	const path = `/project/${userAlias}/${projectAlias}/merge-request`
	return {
		list: () => `${path}/list`,
		single: () => `${path}/${localId}`,
		create: () => `${path}`,
		modify: () => `${path}`, // [ ] уточнить про апи модификации MR
		perform: () => `${path}/${localId}/merge`,
		close: () => `${path}/${localId}/close`,
		cancel: () => `${path}/${localId}/cancel`
	}
}
