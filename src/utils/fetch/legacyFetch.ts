import { axios, _axios } from 'axios-from-npm'
import { Fetch } from './curriedFetch'

export type AxiosRequestConfig = _axios.AxiosRequestConfig<any> | undefined

export interface LegacyProps {
	baseURL: string
	headers?: { Authorization: string }
	options: Fetch['options']
}

export function legacyFetch<T>(
	fetchMethod: Fetch['method'],
	path: string,
	legacyProps: LegacyProps
) {
	const { baseURL, headers, options } = legacyProps
	//TODO clean the code
	// const {
	// 	params: { localId, projectAlias, userAlias, note }
	// } = options as unknown as {
	// 	params: {
	// 		path: string
	// 		localId: 2
	// 		projectAlias: string
	// 		userAlias: string
	// 		note: string
	// 	}
	// }

	console.dir(legacyProps)
	console.dir(path)
	//инстанс axios

	// const response = axios[fetchMethod]<T>(path, { baseURL, headers, ...options })
	//TODO add map object for conditions
	return fetchMethod === 'get'
		? axios.get<T>(path, { baseURL, headers, ...options })
		: axios.post<T>(path, options.params, {
				baseURL,
				headers
				// ...options
		  })

	// return response
}
