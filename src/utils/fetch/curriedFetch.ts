import { GitFlicCredentials } from 'interfaces'
import { legacyFetch, LegacyProps } from './legacyFetch'

export interface Fetch {
	method: 'get' | 'post' | 'put' | 'delete'
	options: { params: { path: string } & unknown }
}

export function curriedFetch({ gitflic_api_url, gitflic_token }: GitFlicCredentials) {
	return function <ReturnType>({ method, options }: Fetch) {
		const { path } = options.params
		const Authorization = `token ${gitflic_token}`
		const legacyProps: LegacyProps = {
			baseURL: gitflic_api_url,
			headers: { Authorization },
			options
		}

		return legacyFetch<ReturnType>(method, path, legacyProps)
	}
}
