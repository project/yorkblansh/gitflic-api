import { OauthParams } from './interfaces/oauth.params.interface'

export class OAUTH {
	private clientId: OauthParams['clientId']
	private scope: OauthParams['scope']
	private state: OauthParams['state']
	private gitflic_oauth_url: OauthParams['gitflic_oauth_url']

	constructor({ clientId, scope, state, gitflic_oauth_url }: OauthParams) {
		this.clientId = clientId
		this.scope = scope
		this.state = state
		this.gitflic_oauth_url = gitflic_oauth_url
	}

	getDeepLink() {
		return (
			`${this.gitflic_oauth_url}` +
			`?scope=${this.scope}` +
			`&clientId=${this.clientId}` +
			`&redirectUrl=https://gitflic.ru/` +
			`&state=${this.state}`
		)
	}

	// async getAccesToken(queryWithAccesCode: string) {
	// 	return await axios.get<AccessToken>(queryWithAccesCode)
	// }
}
