import { AxiosResponse } from 'axios'
import { GitFlic, Project } from '../lib/index'
import { gitflic_api_url, gitflic_token } from './gitflic_credentials.json'

const gf = new GitFlic({
	gitflic_api_url,
	gitflic_token
})

gf.projectQuery
	.select('myProjects')
	.using({
		params: {
			page: 20,
			size: 5
		}
	})
	.build()
	.then((response) => {
		const prArr = response.data._embedded.projectList
		console.dir(prArr) // <- viewing project array
	})
	.catch((err) => console.error(err))

// const getSingleProject = async (): Promise<AxiosResponse<Project, any>> => {
const singleProject = await gf.projectQuery
	.select('singleProject')
	.using({
		userAlias: 'yorkblansh',
		projectAlias: 'gitflic-api'
	})
	.build()

singleProject.data.description
// }
