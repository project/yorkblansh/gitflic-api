import { GitFlic } from '../lib'

const gf = new GitFlic({
	gitflic_api_url: 'https://api.gitflic.ru/',
	gitflic_token: '7efee2d6-1234-4dcb-aad2-ba864598e41a'
})

// selecting >> project query type <<

gf.projectQuery
	.select('singleProject') // select query type
	.using({
		userAlias: 'yorkblansh',
		projectAlias: 'gitflic-api'
	})
	.build()

//done
